<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/users', 'api\UsersController@index');
Route::post('/login', 'api\UsersController@login');
Route::post('/register', 'api\UsersController@register');
Route::post("/featured_jobs", 'api\JobsController@index');
Route::post('/jobs/details', 'api\JobsController@details');
Route::post("/companies", 'api\CompaniesController@index');
Route::post('/companies/details', 'api\CompaniesController@details');
Route::post('/subscribe', 'api\UsersController@subscribe');

Route::get("/set", "api\UsersController@add");
Route::get("/get", "api\CompaniesController@add");

Route::post('/contactus', 'api\HomeController@contactus');
Route::post('/aboutus', 'api\HomeController@aboutus');
Route::post('/faq', 'api\HomeController@faq');

Route::post('/profile', 'api\UsersController@profile');

Route::post('/change_password', 'api\UsersController@change_password');

Route::post('/cities', 'api\JobsController@cities');

Route::post('/categories', 'api\JobsController@categories');