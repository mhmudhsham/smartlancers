<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//Route::get('/sendd', "admin\AdminController@sendd");
Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localize', 'auth']
        ]
        , function() {

    Route::get(LaravelLocalization::transRoute('/settings'), "admin\SettingsController@index");
    Route::post('/settings/store', "admin\SettingsController@store");

    Route::get(LaravelLocalization::transRoute('/admin'), "admin\AdminController@index");

    // accounts
    Route::get(LaravelLocalization::transRoute('/admin/accounts'), "admin\AccountsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/accounts/create'), "admin\AccountsController@create");
    Route::post('/admin/accounts/store', "admin\AccountsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/accounts/edit/{id}'), "admin\AccountsController@edit");
    Route::post('/admin/accounts/update/{id}', "admin\AccountsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/accounts/show/{id}'), "admin\AccountsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/accounts/destroy/{id}'), "admin\AccountsController@destroy");


    Route::get(LaravelLocalization::transRoute('/admin/jobseekers'), "admin\JobseekersController@index");
    Route::get(LaravelLocalization::transRoute('/admin/jobseekers/change_state/{value}/{id}'), "admin\JobseekersController@change_state");
// jobs
    Route::get(LaravelLocalization::transRoute('/admin/jobs'), "admin\JobsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/jobs/change_state/{value}/{id}'), "admin\JobsController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/jobs/change_featured/{value}/{id}'), "admin\JobsController@change_featured");

// orders
    Route::get(LaravelLocalization::transRoute('/admin/orders'), "admin\OrdersController@index");


// paging
    Route::get(LaravelLocalization::transRoute('/admin/pages'), "admin\PagesController@index");

// emails
    Route::get(LaravelLocalization::transRoute('/admin/emails'), "admin\EmailsController@index");


// subscribers
    Route::get(LaravelLocalization::transRoute('/admin/subscribers'), "admin\SubscribersController@index");

// Content fields
    // universities
    Route::get(LaravelLocalization::transRoute('/admin/universities'), "admin\UniversitiesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/universities/create'), "admin\UniversitiesController@create");
    Route::post('/admin/universities/store', "admin\UniversitiesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/universities/edit/{id}'), "admin\UniversitiesController@edit");
    Route::post('/admin/universities/update/{id}', "admin\UniversitiesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/universities/show/{id}'), "admin\UniversitiesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/universities/destroy/{id}'), "admin\UniversitiesController@destroy");

    // cities
    Route::get(LaravelLocalization::transRoute('/admin/cities'), "admin\CitiesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/cities/create'), "admin\CitiesController@create");
    Route::post('/admin/cities/store', "admin\CitiesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/cities/edit/{id}'), "admin\CitiesController@edit");
    Route::post('/admin/cities/update/{id}', "admin\CitiesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/cities/show/{id}'), "admin\CitiesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/cities/destroy/{id}'), "admin\CitiesController@destroy");

    // countries
    Route::get(LaravelLocalization::transRoute('/admin/countries'), "admin\CountriesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/countries/create'), "admin\CountriesController@create");
    Route::post('/admin/countries/store', "admin\CountriesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/countries/edit/{id}'), "admin\CountriesController@edit");
    Route::post('/admin/countries/update/{id}', "admin\CountriesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/countries/show/{id}'), "admin\CountriesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/countries/destroy/{id}'), "admin\CountriesController@destroy");


    // specialists
    Route::get(LaravelLocalization::transRoute('/admin/specialists'), "admin\SpecialistsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/specialists/create'), "admin\SpecialistsController@create");
    Route::post('/admin/specialists/store', "admin\SpecialistsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/specialists/edit/{id}'), "admin\SpecialistsController@edit");
    Route::post('/admin/specialists/update/{id}', "admin\SpecialistsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/specialists/show/{id}'), "admin\SpecialistsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/specialists/destroy/{id}'), "admin\SpecialistsController@destroy");


    // industries
    Route::get(LaravelLocalization::transRoute('/admin/industries'), "admin\IndustriesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/industries/create'), "admin\IndustriesController@create");
    Route::post('/admin/industries/store', "admin\IndustriesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/industries/edit/{id}'), "admin\IndustriesController@edit");
    Route::post('/admin/industries/update/{id}', "admin\IndustriesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/industries/show/{id}'), "admin\IndustriesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/industries/destroy/{id}'), "admin\IndustriesController@destroy");


    // jobtypes
    Route::get(LaravelLocalization::transRoute('/admin/jobtypes'), "admin\JobtypesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/jobtypes/create'), "admin\JobtypesController@create");
    Route::post('/admin/jobtypes/store', "admin\JobtypesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/jobtypes/edit/{id}'), "admin\JobtypesController@edit");
    Route::post('/admin/jobtypes/update/{id}', "admin\JobtypesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/jobtypes/show/{id}'), "admin\JobtypesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/jobtypes/destroy/{id}'), "admin\JobtypesController@destroy");


// faqtypes
    Route::get(LaravelLocalization::transRoute('/admin/faqtypes'), "admin\FaqtypesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/faqtypes/create'), "admin\FaqtypesController@create");
    Route::post('/admin/faqtypes/store', "admin\FaqtypesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/faqtypes/edit/{id}'), "admin\FaqtypesController@edit");
    Route::post('/admin/faqtypes/update/{id}', "admin\FaqtypesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/faqtypes/show/{id}'), "admin\FaqtypesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/faqtypes/destroy/{id}'), "admin\FaqtypesController@destroy");



    // majors
    Route::get(LaravelLocalization::transRoute('/admin/majors'), "admin\MajorsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/majors/create'), "admin\MajorsController@create");
    Route::post('/admin/majors/store', "admin\MajorsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/majors/edit/{id}'), "admin\MajorsController@edit");
    Route::post('/admin/majors/update/{id}', "admin\MajorsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/majors/show/{id}'), "admin\MajorsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/majors/destroy/{id}'), "admin\MajorsController@destroy");


    // instructors
    Route::get(LaravelLocalization::transRoute('/admin/instructors'), "admin\InstructorsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/instructors/courses/{instructor_id}'), "admin\InstructorsController@courses");
    Route::get(LaravelLocalization::transRoute('/admin/instructors/create'), "admin\InstructorsController@create");
    Route::post('/admin/instructors/store', "admin\InstructorsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/instructors/edit/{id}'), "admin\InstructorsController@edit");
    Route::post('/admin/instructors/update/{id}', "admin\InstructorsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/instructors/show/{id}'), "admin\InstructorsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/instructors/destroy/{id}'), "admin\InstructorsController@destroy");




// companytypes
    Route::get(LaravelLocalization::transRoute('/admin/companytypes'), "admin\CompanytypesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/companytypes/create'), "admin\CompanytypesController@create");
    Route::post('/admin/companytypes/store', "admin\CompanytypesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/companytypes/edit/{id}'), "admin\CompanytypesController@edit");
    Route::post('/admin/companytypes/update/{id}', "admin\CompanytypesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/companytypes/show/{id}'), "admin\CompanytypesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/companytypes/destroy/{id}'), "admin\CompanytypesController@destroy");



// portaltags
    Route::get(LaravelLocalization::transRoute('/admin/portaltags'), "admin\PortaltagsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/portaltags/create'), "admin\PortaltagsController@create");
    Route::post('/admin/portaltags/store', "admin\PortaltagsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/portaltags/edit/{id}'), "admin\PortaltagsController@edit");
    Route::post('/admin/portaltags/update/{id}', "admin\PortaltagsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/portaltags/show/{id}'), "admin\PortaltagsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/portaltags/destroy/{id}'), "admin\PortaltagsController@destroy");


// companyemployees
    Route::get(LaravelLocalization::transRoute('/admin/companyemployees'), "admin\CompanyemployeesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/companyemployees/create'), "admin\CompanyemployeesController@create");
    Route::post('/admin/companyemployees/store', "admin\CompanyemployeesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/companyemployees/edit/{id}'), "admin\CompanyemployeesController@edit");
    Route::post('/admin/companyemployees/update/{id}', "admin\PortaltagsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/companyemployees/show/{id}'), "admin\CompanyemployeesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/companyemployees/destroy/{id}'), "admin\CompanyemployeesController@destroy");

// genders
    Route::get(LaravelLocalization::transRoute('/admin/genders'), "admin\GendersController@index");
    Route::get(LaravelLocalization::transRoute('/admin/genders/create'), "admin\GendersController@create");
    Route::post('/admin/genders/store', "admin\GendersController@store");
    Route::get(LaravelLocalization::transRoute('/admin/genders/edit/{id}'), "admin\GendersController@edit");
    Route::post('/admin/genders/update/{id}', "admin\GendersController@update");
    Route::get(LaravelLocalization::transRoute('/admin/genders/show/{id}'), "admin\GendersController@show");
    Route::get(LaravelLocalization::transRoute('/admin/genders/destroy/{id}'), "admin\GendersController@destroy");


// graduationyears
    Route::get(LaravelLocalization::transRoute('/admin/graduationyears'), "admin\GraduationyearsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/graduationyears/create'), "admin\GraduationyearsController@create");
    Route::post('/admin/graduationyears/store', "admin\GraduationyearsController@store");
    Route::get(LaravelLocalization::transRoute('/admin/graduationyears/edit/{id}'), "admin\GraduationyearsController@edit");
    Route::post('/admin/graduationyears/update/{id}', "admin\GraduationyearsController@update");
    Route::get(LaravelLocalization::transRoute('/admin/graduationyears/show/{id}'), "admin\GraduationyearsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/graduationyears/destroy/{id}'), "admin\GraduationyearsController@destroy");



// newsletters
    Route::get(LaravelLocalization::transRoute('/admin/newsletters'), "admin\NewslettersController@index");
    Route::get(LaravelLocalization::transRoute('/admin/newsletters/create'), "admin\NewslettersController@create");
    Route::post('/admin/newsletters/store', "admin\NewslettersController@store");
    Route::get(LaravelLocalization::transRoute('/admin/newsletters/edit/{id}'), "admin\NewslettersController@edit");
    Route::post('/admin/newsletters/update/{id}', "admin\NewslettersController@update");
    Route::get(LaravelLocalization::transRoute('/admin/newsletters/show/{id}'), "admin\NewslettersController@show");
    Route::get(LaravelLocalization::transRoute('/admin/newsletters/destroy/{id}'), "admin\NewslettersController@destroy");



// FAQ
    Route::get(LaravelLocalization::transRoute('/admin/faq'), "admin\FAQController@index");
    Route::get(LaravelLocalization::transRoute('/admin/faq/create'), "admin\FAQController@create");
    Route::post('/admin/faq/store', "admin\FAQController@store");
    Route::get(LaravelLocalization::transRoute('/admin/faq/edit/{id}'), "admin\FAQController@edit");
    Route::post('/admin/faq/update/{id}', "admin\FAQController@update");
    Route::get(LaravelLocalization::transRoute('/admin/faq/show/{id}'), "admin\FAQController@show");
    Route::get(LaravelLocalization::transRoute('/admin/faq/destroy/{id}'), "admin\FAQController@destroy");


// companies
    Route::get(LaravelLocalization::transRoute('/admin/companies'), "admin\CompaniesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/companies/change_state/{value}/{id}'), "admin\CompaniesController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/companies/create'), "admin\CompaniesController@create");
    Route::post('/admin/companies/store', "admin\CompaniesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/companies/edit/{id}'), "admin\CompaniesController@edit");
    Route::post('/admin/companies/update/{id}', "admin\CompaniesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/companies/show/{id}'), "admin\CompaniesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/companies/destroy/{id}'), "admin\CompaniesController@destroy");


// courses
    Route::get(LaravelLocalization::transRoute('/admin/courses'), "admin\CoursesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/courses/change_state/{value}/{id}'), "admin\CoursesController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/courses/change_featured/{value}/{id}'), "admin\CoursesController@change_featured");
    Route::get(LaravelLocalization::transRoute('/admin/courses/view/{id}'), "admin\CoursesController@show");


// camps
    Route::get(LaravelLocalization::transRoute('/admin/camps'), "admin\CampsController@index");
    Route::get(LaravelLocalization::transRoute('/admin/camps/change_state/{value}/{id}'), "admin\CampsController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/camps/view/{id}'), "admin\CampsController@show");
    Route::get(LaravelLocalization::transRoute('/admin/camp_reservations'), 'admin\CampsController@camp_reservations');


// materials
    Route::get(LaravelLocalization::transRoute('/admin/materials'), "admin\MaterialsController@index");




// pricing
    Route::get(LaravelLocalization::transRoute('/admin/pricing'), "admin\PricingController@index");
    Route::get(LaravelLocalization::transRoute('/admin/pricing/change_state/{value}/{id}'), "admin\PricingController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/pricing/create'), "admin\PricingController@create");
    Route::post('/admin/pricing/store', "admin\PricingController@store");
    Route::get(LaravelLocalization::transRoute('/admin/pricing/edit/{id}'), "admin\PricingController@edit");
    Route::post('/admin/pricing/update/{id}', "admin\PricingController@update");
    Route::get(LaravelLocalization::transRoute('/admin/pricing/show/{id}'), "admin\PricingController@show");
    Route::get(LaravelLocalization::transRoute('/admin/pricing/destroy/{id}'), "admin\PricingController@destroy");



// category
    Route::get(LaravelLocalization::transRoute('/admin/categories'), "admin\CategoriesController@index");
    Route::get(LaravelLocalization::transRoute('/admin/categories/change_state/{value}/{id}'), "admin\CategoriesController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/categories/change_state/{value}/{id}'), "admin\CategoriesController@change_state");
    Route::get(LaravelLocalization::transRoute('/admin/categories/create'), "admin\CategoriesController@create");
    Route::post('/admin/categories/store', "admin\CategoriesController@store");
    Route::get(LaravelLocalization::transRoute('/admin/categories/edit/{id}'), "admin\CategoriesController@edit");
    Route::post('/admin/categories/update/{id}', "admin\CategoriesController@update");
    Route::get(LaravelLocalization::transRoute('/admin/categories/show/{id}'), "admin\CategoriesController@show");
    Route::get(LaravelLocalization::transRoute('/admin/categories/destroy/{id}'), "admin\CategoriesController@destroy");
});

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localize']
        ]
        , function() {
    Route::get(LaravelLocalization::transRoute('/'), "HomeController@index");
    Route::get(LaravelLocalization::transRoute('/faq'), "HomeController@faq");
    Route::get(LaravelLocalization::transRoute('/contact'), "HomeController@contact");
    Route::post("/send", "HomeController@send");

    // companies
    Route::get(LaravelLocalization::transRoute('/companies'), "CompaniesController@index");
    Route::get(LaravelLocalization::transRoute('/companies/details/{id}/{title}'), "CompaniesController@details");

    // jobs
    Route::get(LaravelLocalization::transRoute('/jobs'), "JobsController@index");
    Route::get(LaravelLocalization::transRoute('/jobs/details/{id}/{title}'), "JobsController@details");
    Route::post('/search_jobs', "JobsController@search_jobs");
    Route::post('/filter_jobs_ajax', "JobsController@filter_jobs_ajax");

    Route::post("/subscribe", "HomeController@subscribe");
    Route::post("/client_login", "HomeController@client_login");
    Route::post("/client_register", "HomeController@client_register");
    Route::get(LaravelLocalization::transRoute('/profile'), "HomeController@profile");
    Route::get(LaravelLocalization::transRoute('/client_logout'), "HomeController@client_logout");
    Route::get(LaravelLocalization::transRoute('/client_jobs'), "HomeController@client_jobs");
    Route::get(LaravelLocalization::transRoute('/change_password'), "HomeController@change_password");

    Route::post('/store_password', "HomeController@store_password");
    Route::post('/change_profile', "HomeController@change_profile");

    Route::get('/client_apply', 'HomeController@client_apply');
    Route::get('/check_apply', 'HomeController@check_apply');

    Route::get(LaravelLocalization::transRoute('/courses/add'), 'CoursesController@add_course');
    Route::post('/courses/store', 'CoursesController@store_course');
    Route::get(LaravelLocalization::transRoute('/courses'), 'CoursesController@index');
    Route::get(LaravelLocalization::transRoute('/courses/details/{id}'), 'CoursesController@details');
    Route::post('/courses/book', 'CoursesController@book');

    Route::get(LaravelLocalization::transRoute('/camps/add'), 'CampsController@add_camp');
    Route::post('/camps/store', 'CampsController@store_camp');
    Route::get(LaravelLocalization::transRoute('/camps'), 'CampsController@index');
    Route::get(LaravelLocalization::transRoute('/camps/details/{id}'), 'CampsController@details');
    Route::post('/camps/book', 'CampsController@book');

    Route::post("/instructor_register", "InstructorsController@add");
    Route::get(LaravelLocalization::transRoute("/instructor"), "InstructorsController@instructor");

    Route::post("/employer_register", "CompaniesController@add");
    Route::get(LaravelLocalization::transRoute("/company"), "CompaniesController@company");

    Route::get(LaravelLocalization::transRoute("/post_job"), "CompaniesController@post_job");
    Route::post('/update_company_profile', "CompaniesController@update_company_profile");

    Route::post('/store_job', 'CompaniesController@store_job');
    
});
Auth::routes();

Route::get('/updateapp', function()
{
    exec('composer dump-autoload');
    echo 'dump-autoload complete';
});


Route::get('/add', 'TestController@index');
Route::get('/get', 'TestsController@print');