//var types = new Array();
var Job = function() {

    var manageFilter = function() {
        $(document).on("click", ".job_type", function(el) {
            var types = new Array();
            $(".job_type").each(function() {
                if ($(this).is(":checked")) {
                    var type_id = $(this).data("id");
                    types.push(type_id);
                }
            });
            $.ajax({
                url: config.site_url + "/filter_jobs_ajax",
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    types: types
                },
                success: function(msg) {
                    console.log(msg);
                    $("#all_jobs").html(msg);
                }
            });
        });
    };

    var checkApplied = function() {
        var job_id = $("#job-id").val();
        $.ajax({
            url: config.site_url + "/check_apply",
            type: "get",
            data: {
                job_id: job_id
            },
            success: function(msg) {
                if (msg == 1) {
                    $("#apply-for-job").html(lang.successfully_applied);
                } else {
                    $("#apply-for-job").html(lang.login_first);
                }
            }
        });
    };

    var applyJob = function() {
        $(document).on("click", "#apply-for-job", function(e) {
            e.preventDefault();
            var job_id = $("#job-id").val();
            $.ajax({
                url: config.site_url + "/client_apply",
                type: "get",
                data: {
                    job_id: job_id
                },
                success: function(msg) {
                    if (msg == 1) {
                        $("#apply-for-job").html(lang.successfully_applied);
                    } else {
                        $("#apply-for-job").html(lang.login_first);
                    }
                }
            });
        });
    };

    return {
        init: function() {
            var hidden_value = $("#hidden-field").val() || "";
            if (hidden_value != "") {
                manageFilter();
            } else {
                checkApplied();
                applyJob();
            }
        }
    };
}();

$(document).ready(function() {
    Job.init();
});