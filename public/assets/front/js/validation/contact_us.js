$(document).ready(function() {
    $("#contact_us_form").submit(function(e) {
        e.preventDefault();
        var flag = true;
        $(".required_field").each(function() {
            var $this = $(this);
            if ($this === "") {
                $this.closest(".pf-field").find(".help-block").html(lang.this_field_is_required);
                flag = false;
            }
        });

        if (flag) {
            var formData = $(this).serialize();
            $.ajax({
                url: config.base_url + "/send",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                data: formData,
                success: function(msg) {
                    console.log(msg);
                    if (msg === 1) {
                        $("button[type='submit']").html(lang.sent_successfully);
                        $("button[type='submit']").attr("disabled", "disabled");
                    }
                }
            });
        }
    });
});