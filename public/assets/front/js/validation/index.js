var Index = function() {
    var subscribeForm = function() {
        $("#subscribe_form").submit(function(e) {
            e.preventDefault();
            var email = $("#subscribe_form input[type='text']");
            if (email.val() === "") {
                $("#help-block-subscribe").css("color", "red").css("margin-top", "10px").html(lang.this_field_is_required);
                setTimeout(function() {
                    $("#help-block-subscribe").css("color", "red").html("");
                }, 1500);
            } else {
                $.ajax({
                    url: config.site_url + "/subscribe",
                    type: "post",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {email: email.val()},
                    success: function(msg) {
                        console.log(msg);
                        if (msg === 0) {
                            $("#help-block-subscribe").css("color", "red").html(lang.already_exists);
                        } else {
                            $("#help-block-subscribe").css("color", "red").html(lang.sent_successfully);
                        }
                    }
                });
            }
        });
    };
    return {
        init: function() {
            subscribeForm();
        }
    };
}();

$(document).ready(function() {
    Index.init();
});