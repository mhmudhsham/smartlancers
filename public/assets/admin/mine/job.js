var Job = function() {
    var init = function() {
        changeStatus();
        changeFeatured();
    };

    var changeStatus = function() {
        $(document).on("click", ".status-checkbox", function() {
            var $this = $(this);
            var $value = $(this).val();
            var $id = $(this).data("id");
            $.ajax({
                url: config.admin_url + "/jobs/change_state" + "/" + $value + "/" + $id,
                type: "get",
                data: {},
                success: function(msg) {
                    if (msg == 1) {
                        if ($value == 1)
                            $this.val(0);
                        else
                            $this.val(1);
                        toastr.success("Changed Successfully");
                    } else {
                        toastr.error("Error in updating");
                    }
                }
            });
        });
    };
    var changeFeatured = function() {
        $(document).on("click", ".featured-checkbox", function() {
            var $this = $(this);
            var $value = $(this).val();
            var $id = $(this).data("id");
            $.ajax({
                url: config.admin_url + "/jobs/change_featured" + "/" + $value + "/" + $id,
                type: "get",
                data: {},
                success: function(msg) {
                    if (msg == 1) {
                        if ($value == 1)
                            $this.val(0);
                        else
                            $this.val(1);
                        toastr.success("Changed Successfully");
                    } else {
                        toastr.error("Error in updating");
                    }
                }
            });
        });
    };

    return {
        init: function() {
            init();
        }
    };
}();

$(document).ready(function() {
    Job.init();
});