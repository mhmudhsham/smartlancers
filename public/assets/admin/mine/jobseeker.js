$(document).on("click", ".status-checkbox", function() {
    var $this = $(this);
    var $value = $(this).val();
    var $id = $(this).data("id");
    $.ajax({
        url: config.admin_url + "/jobseekers/change_state" + "/" + $value + "/" + $id,
        type: "get",
        data: {},
        success: function(msg) {
            if (msg == 1) {
                if ($value == 1)
                    $this.val(0);
                else
                    $this.val(1);
                toastr.success("Changed Successfully");
            } else {
                toastr.error("Error in updating");
            }
        }
    });
});