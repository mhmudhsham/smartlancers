var Settings = function() {
    var init = function() {
        validateCreateForm();
    };
    var validateCreateForm = function() {
        var form1 = $('#settings_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                site_title_ar: {
                    required: lang.this_field_is_required
                },
                site_title_en: {
                    required: lang.this_field_is_required
                },
                currency_ar: {
                    required: lang.this_field_is_required
                },
                currency_en: {
                    required: lang.this_field_is_required
                },
                site_keywords_ar: {
                    required: lang.this_field_is_required
                },
                site_keywords_en: {
                    required: lang.this_field_is_required
                },
                site_description_ar: {
                    required: lang.this_field_is_required
                },
                site_description_en: {
                    required: lang.this_field_is_required
                },
                facebook: {
                    url: lang.url_not_true
                },
                twitter: {
                    url: lang.url_not_true
                },
                youtube: {
                    url: lang.url_not_true
                },
                linkedin: {
                    url: lang.url_not_true
                }
            },
            rules: {
                site_title_ar: {
                    required: true
                },
                site_title_en: {
                    required: true
                },
                currency_ar: {
                    required: true
                },
                currency_en: {
                    required: true
                },
                site_keywords_ar: {
                    required: true
                },
                site_keywords_en: {
                    required: true
                },
                site_description_ar: {
                    required: true
                },
                site_description_en: {
                    required: true
                },
                facebook: {
                    url: true
                },
                twitter: {
                    url: true
                },
                youtube: {
                    url: true
                },
                linkedin: {
                    url: true
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element); // for other inputs, just perform default behavior
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                success1.show();
                error1.hide();
                form.submit();
            }
        });
    };
    return {
        init: function() {
            init();
        }
    };
}();
$(document).ready(function() {
    Settings.init();
});