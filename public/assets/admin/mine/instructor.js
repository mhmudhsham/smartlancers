var Instructor = function() {
    var init = function() {
        var id = $("#id").val() || "";
        if (id !== "") {
            validateCreateForm();
            if (id !== 0) {
                handleFormValues(id);
            }
        }
        deleteURL();
    };


    var deleteURL = function() {
        $(document).on("click", ".delete-url", function(e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            var href = $(this).attr("href");
            $.ajax({
                url: href,
                type: "get",
                data: {},
                success: function(msg) {
                    $(".tr_" + id).fadeOut(500);
                    toastr.success(lang.deleted_successfully);
                }
            });
        });
    };

    var handleFormValues = function(id) {
        $.ajax({
            url: config.admin_url + "/instructors/show/" + id,
            type: "get",
            data: {},
            success: function(msg) {
                for (var i in msg) {
                    if (i === "image") {
                        var logo = config.base_url + "/uploads/instructors/" + msg[i];
                        $(".thumbnail").html("<img src='" + logo + "' />");
                    } else {
                        $("#" + i).val(msg[i]);
                    }
                }
            }
        });
    };

    var validateCreateForm = function() {
        var form1 = $('#instructor_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                name: {
                    required: lang.this_field_is_required
                },
                email: {
                    required: lang.this_field_is_required,
                    email: lang.email_not_valid
                },
                phone: {
                    number: lang.not_number
                },
                username: {
                    required: lang.this_field_is_required
                },
                password: {
                    required: lang.this_field_is_required
                }
            },
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    number: true
                },
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            errorPlacement: function(error, element) {
                error.insertAfter(element); // for other inputs, just perform default behavior
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                success1.show();
                error1.hide();
                form.submit();
            }
        });
    };
    return {
        init: function() {
            init();
        }
    };
}();
$(document).ready(function() {
    Instructor.init();
});