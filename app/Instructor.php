<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model {

    public $table = "instructors";

    public function courses() {
        return $this->hasMany("App\Course", "instructor_id");
    }

}
