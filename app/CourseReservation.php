<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseReservation extends Model {

    public $table = "courses_reservations";

    public function course() {
        return $this->belongsTo("App\Course", "course_id");
    }

}
