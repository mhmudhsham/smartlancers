<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model {

    public $table = "materials";

    public function course() {
        return $this->belongsTo("App\Course", "course_id");
    }

}
