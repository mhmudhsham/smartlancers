<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQ extends Model {

    public $table = "faq";

    public function faqtype() {
        return $this->belongsTo("App\Faqtype", "faqtype_id");
    }

}
