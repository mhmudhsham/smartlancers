<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    public $table = "cities";

    public function companies() {
        return $this->hasMany("App\Company", "city_id");
    }

}
