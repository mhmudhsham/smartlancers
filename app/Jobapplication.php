<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobapplication extends Model {

    public $table = "job_applications";

    public function job() {
        return $this->belongsTo("App\Job", "job_id");
    }

}
