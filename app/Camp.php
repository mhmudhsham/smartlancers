<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camp extends Model {

    public $table = "camps";

    public function jobs() {
        return $this->hasMany("App\CampReservation", "camp_id");
    }


}
