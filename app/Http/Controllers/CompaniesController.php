<?php

namespace App\Http\Controllers;

use Validator;
use Session;
use Illuminate\Http\Request;

class CompaniesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $companies = \App\Company::with("city")->withCount("jobs")->where("status", "!=", 0)->get();
        return view('front.companies.index', compact("companies"));
    }

    public function details($id, $title) {
        $details = \App\Company::find($id);
        $jobs = \App\Job::with("jobtype")->where("com_id", $id)->get();
        return view("front.companies.details", compact("details", "jobs"));
    }

    public function add(Request $request) { 
        $company = new \App\Company;
        $company->user_fullname = $request->username;
        $company->user_password = md5($request->password);
        $company->user_email = $request->email;
        $company->phone_number = $request->phone;
        $company->priceplan_id = $company->status = $company->companyemployee_id = $company->companytype_id = $company->the_order = 0;        
        $company->title_ar = "";
        $company->title_en = "";
        $company->description = "";
        $company->email = "";
        $company->website = "";
        $company->save();
        $id = $company->id;
        Session::put("user_id", $id);
        Session::put("logged", 1);
        Session::put("company", 1);
        Session::save();
        $data = ["id" => $id, "redirect" => "company", "key" => "user_id"];
        $company_data = json_encode($data);
        echo $company_data;
        die();
    }

    public function company() {
        $company_id = Session::get("user_id");
        $details = \App\Company::where("id", $company_id)->first();
        return view("front.companies.profile", compact("details"));
    }

    public function post_job() {
        $categories = \App\Category::all();
        $jobtypes = \App\Jobtype::all();
        return view("front.companies.post_job", compact('categories', 'jobtypes'));
    }

    public function store_job(Request $request) {
        $validator = Validator::make($request->all(), [
            'job_title' => 'required',
            'salary' => 'required',
            'working_hours' => 'required', 
        ]);
        if ($validator->fails()) {
            return redirect($this->_lang . "/contact")
                            ->withErrors($validator)
                            ->withInput();
        }


        $job = new \App\Job;
        $job->job_title = $request->job_title;
        $job->salary = $request->salary;
        $job->working_hours = $request->working_hours;
        $job->category_id = $request->category_id;
        $job->job_location = $request->job_location; 
        $job->short_desc = $request->short_desc;
        $job->job_type_id = $request->job_type_id;
        $job->price_package_id = $job->status = 0;
        $job->package_expirydate = $job->dateadded = 0;
        $job->application_email = "";
        $job->gender = $request->gender;
        $job->responsibilties = $job->skills_required = "";
        
        $job->com_id = Session::get("user_id");
        $job->job_sef = "";
        $job->save();     
        
        if ($this->_lang == "en") {
            return redirect('/company')
                            ->withErrors(["Added successfully"]);
        } else {
            return redirect('/company')
                            ->withErrors(["تم الإضافة بنجاح"]);
        }
    }


    public function update_company_profile(Request $request) {
        $validator = Validator::make($request->all(), [
            'logo' => 'required',
        ]);

        if ($validator->fails()) {
            if ($this->_lang == "en") {
                return redirect('/company')
                                ->withErrors(["Image is required"]);
            } else {
                return redirect('/company')
                                ->withErrors(["الصورة مقبولة"]);
            }
        }

        $image = $request->file('logo');
        $logo = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/companies');
        $image->move($destinationPath, $logo);

        $user_id = Session::get("user_id");
        $data = \App\Company::find($user_id);
        $data->logo = $logo;
        $data->save();

        if ($this->_lang == "en") {
            return redirect('/company')
                            ->withErrors(["updated successfully"]);
        } else {
            return redirect('/company')
                            ->withErrors(["تم التحديث بنجاح"]);
        }
    }

}
