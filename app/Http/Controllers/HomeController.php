<?php

namespace App\Http\Controllers;

use Session;
use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $cities = \App\City::all();
        $jobs = \App\Job::with("jobtype")->with("city")->with("company")->where("status", "!=", 0)->where("featured", 1)->get();
        return view('front.index', compact("jobs", "cities"));
    }

    public function faq() {
        $faq = \App\FAQ::orderBy("id", "desc")->get();
        return view("front.faq", compact("faq"));
    }

    public function contact() {
        return view("front.contact");
    }

    public function send(Request $request) {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
                    'full_name' => 'required',
                    'email' => 'required',
                    'subject' => 'required',
                    'message' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect($this->_lang . "/contact")
                            ->withErrors($validator)
                            ->withInput();
        }

        $message = new \App\Message;
        $message->full_name = $request->full_name;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $result = $message->save();
        echo $result;
        die();
    }

    public function subscribe(Request $request) {
        $email = \App\Subscribe::where("sub_email", $request->email)->get()->first();
        if ($email != null) {
            echo 0;
            die();
        }
        $subscribe = new \App\Subscribe;
        $subscribe->sub_email = $request->email;
        $subscribe->dateadded = time();
        $subscribe->save();
        echo 1;
        die();
    }

    public function client_login(Request $request) {
        $username = $request->username;
        $password = md5($request->password);
        $data = \App\Jobseeker::where("email", $username)->where("password", $password)->where("status", 1)->get()->first();
// dd($data)        ;
        if ($data != null) {
            Session::put("user_id", $data->id);
            Session::save();
            echo $data->id;
            die();
        } else {
            echo 0;
            die();
        }
    }

    public function profile() {
        $universities = \App\University::all();
        $countries = \App\Country::all();
        $cities = \App\City::all();
        $user_id = Session::get("user_id");
        $details = \App\Jobseeker::find($user_id);
        if ($user_id == "" || $user_id == null) {
            return redirect(url($this->_lang . '/'));
        }
        return view("front.users.profile", compact('details', "universities", "countries", "cities"));
    }

    public function client_logout() {
        Session::put("user_id", "");
        return redirect(url($this->_lang . '/'));
    }

    public function client_jobs() {
        $user_id = Session::get("user_id");
        $jobs = \App\Jobapplication::with("job")->where("jobseeker_id", $user_id)->get();
        return view("front.users.jobs", compact("jobs"));
    }

    public function change_password() {
        return view("front.users.change_password");
    }

    public function store_password(Request $request) {
        $validator = Validator::make($request->all(), [
                    'old_password' => 'required',
                    'new_password' => 'required',
                    'confirm_password' => 'required',
        ]);
        if ($validator->fails()) {
            if ($this->_lang == "en") {
                return redirect('/change_password')
                                ->withErrors(['fill all fields']);
            } else {
                return redirect('/change_password')
                                ->withErrors(["املأ كل الحقول"]);
            }
        }
        $user_id = Session::get("user_id");
        $data = \App\Jobseeker::find($user_id);
        $old_password = md5($request->old_password);
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;
        if ($data->password != $old_password) {
            if ($this->_lang == "en") {
                return redirect('/change_password')
                                ->withErrors(["Old password not true"]);
            } else {
                return redirect('/change_password')
                                ->withErrors(["كلمة السر القديمة غير صحيحة"]);
            }
        }
        if ($new_password != $confirm_password) {
            if ($this->_lang == "en") {
                return redirect('/change_password')
                                ->withErrors(["passwords_not_equal"]);
            } else {
                return redirect('/change_password')
                                ->withErrors(["كلمتا السر غير متطابقتان"]);
            }
        }
        $data->password = md5($request->new_password);
        $data->save();
        if ($this->_lang == "en") {
            return redirect('/change_password')
                            ->withErrors(["updated successfully"]);
        } else {
            return redirect('/change_password')
                            ->withErrors(["تم التحديث بنجاح"]);
        }
    }

    public function change_profile(Request $request) {
        $validator = Validator::make($request->all(), [
                    'logo' => 'required',
        ]);

        if ($validator->fails()) {
            if ($this->_lang == "en") {
                return redirect('/profile')
                                ->withErrors(["Image is required"]);
            } else {
                return redirect('/profile')
                                ->withErrors(["الصورة مقبولة"]);
            }
        }

        $image = $request->file('logo');
        $logo = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/jobseekers');
        $image->move($destinationPath, $logo);

        $user_id = Session::get("user_id");
        $data = \App\Jobseeker::find($user_id);
        $data->image = $logo;
        $data->save();

        if ($this->_lang == "en") {
            return redirect('/profile')
                            ->withErrors(["updated successfully"]);
        } else {
            return redirect('/profile')
                            ->withErrors(["تم التحديث بنجاح"]);
        }
    }

    public function client_register(Request $request) {
        $username = $request->username;
        $password = md5($request->password);
        $email = $request->email;
        $phone = $request->phone;
        // get count job seekers of this mail
        $data = \App\Jobseeker::where("email", $email)->count();
        if ($data > 0) {
            echo 0;
            die();
        }
        $jobseeker = new \App\Jobseeker;
        $jobseeker->username = $username;
        $jobseeker->password = $password;
        $jobseeker->email = $email;
        $jobseeker->phone = $phone;
        $jobseeker->save();
        $id = $jobseeker->id;
        Session::put("user_id", $id);
        Session::put("logged", 1);
        Session::put("company", 0);
        Session::save();
        echo $id;
        die();
    }

    public function client_apply(Request $request) {
        $job_id = $request->job_id;
        $user_id = Session::get("user_id");
        if ($user_id != "" && $user_id != null) {
            $count = \App\Jobapplication::where("jobseeker_id", $user_id)->where("job_id", $job_id)->count();
            if ($count == 0) {
                $job_app = new \App\Jobapplication;
                $job_app->jobseeker_id = $user_id;
                $job_app->job_id = $job_id;
                $job_app->status = 0;
                $job_app->dateadded = time();
                $job_app->accepted_date = 0;
                $job_app->rejected_date = 0;
                $job_app->save();
            }
            echo 1;
            die();
        }
        echo 0;
        die();
    }

    public function check_apply(Request $request) {
        $job_id = $request->job_id;
        $user_id = Session::get("user_id");
        if ($user_id != "" && $user_id != null) {
            $count = \App\Jobapplication::where("jobseeker_id", $user_id)->where("job_id", $job_id)->count();
            if ($count > 0) {
                echo 1;
                die();
            } else {
                echo 0;
                die();
            }
        }
    }

}
