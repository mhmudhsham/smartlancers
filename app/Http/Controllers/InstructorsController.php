<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Validator;
use \App\Instructor;

class InstructorsController extends Controller {
    
    public function __construct() {
        parent::__construct();
    }

    public function add(Request $request) { 
        $instructor = new Instructor;
        $instructor->username = $request->username;
        $instructor->password = md5($request->password);
        $instructor->email = $request->email;
        $instructor->phone = $request->phone;
        $instructor->save();
        $id = $instructor->id;
        Session::put("user_id", $id);
        Session::put("logged", 1);
        Session::put("company", 0);
        Session::save();
        $data = ["id" => $id, "redirect" => "instructor", "key" => "user_id"];
        $instructor_data = json_encode($data);
        echo $instructor_data;
        die();
    }

    public function instructor() {
        $instructor_id = Session::get("user_id");
        $details = Instructor::where("id", $instructor_id)->first();
        return view("front.instructors.profile", compact("details"));
    }

}