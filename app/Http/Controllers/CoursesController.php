<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Course;

class CoursesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function add_course() {
        $majors = \App\Major::all();
        return view("front.courses.add", compact('majors'));
    }

    public function store_course(Request $request) {
        $validator = Validator::make($request->all(), [
            'title_ar' => 'required|max:255',
            'title_en' => 'required|max:255',
            'description_ar' => "required",
            "description_en" => "required",
            "total_hours" => "required",
            "price" => "required",
            "major_id" => "required",
            "session_total_hours" => "required",
            "start_date" => "required"            
        ]);
        if ($validator->fails()) {
            return redirect('courses/add')
                            ->withErrors($validator)
                            ->withInput();
        }


        $image = $request->file('logo');
        $logo = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/courses');
        $image->move($destinationPath, $logo);

        $course = new Course;
        $course->logo = $logo;
        $course->title_ar = $request->title_ar;
        $course->title_en = $request->title_en;
        $course->description_ar = $request->description_ar;
        $course->description_en = $request->description_en;
        $course->total_hours = $request->total_hours;
        $course->price = $request->price;
        $course->cash_type = $request->cash_type;
        $course->attendance_type = $request->attendance_type;
        $course->major_id = $request->major_id;  //               
        $course->session_total_hours = $request->session_total_hours;
        $course->start_date = $request->start_date;
        $course->instructor_id = 0;
        $course->total_students = 0;
        $course->is_active = 0;
        $course->is_featured = 0;
        $course->attendance_days = "";
        $course->attendance_hours = "";
        $course->save();
        
        return redirect('courses/add')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
        
    }    

    public function index() {
        $courses = Course::orderBy("id", "desc")->where("is_active", 1)->paginate(16);            
        return view('front.courses.index', compact('courses'));
    }

    public function details($id) {    
        $details = Course::find($id);
        return view("front.courses.view", compact('details'));
    }


    public function book(Request $request) {
        // dd($request->all());
        $course_id = $request->course_id;
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => "required",
            "message" => "required",           
        ]);
        if ($validator->fails()) {
            return redirect($this->_lang.'/courses/details/'.$course_id)
                            ->withErrors($validator)
                            ->withInput();
        } 

        $course = new \App\CourseReservation;       
        $course->name = $request->name;
        $course->course_id = $request->course_id;
        $course->email = $request->email;
        $course->phone = $request->phone;
        $course->message = $request->message;
        $course->save();

        return redirect()->back()->with('success', 'Your request sent successfully');   
//         return redirect($this->_lang.'/courses/details/'.$course_id)->with(
//             'info', 'Permission has been added to your Collaborator successfully'
// );
    }
}
