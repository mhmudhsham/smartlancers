<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JobsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $cities = \App\City::all();
        $types = \App\Jobtype::all();
        $jobs = \App\Job::with("jobtype")->with("city")->with("company")->where("status", "!=", 0)->get();
        return view('front.jobs.index', compact("jobs", "cities", 'types'));
    }

    public function details($id, $title) {
        $details = \App\Job::with("jobtype")->with('city')->with("company")->find($id);
//        dd($details);
        return view("front.jobs.details", compact("details"));
    }

    public function search_jobs(Request $request) {
        $cities = \App\City::all();
        $location = $request->location;
        $title = $request->title;
        $jobs = \App\Job::
                with("jobtype")
                ->with("city")
                ->with("company")
                ->where("status", "!=", 0)
                ->where("job_title", "LIKE", "%$title%")
                ->where("job_location", $location)
                ->get();
        return view('front.jobs.index', compact("jobs", "cities"));
    }

    public function filter_jobs_ajax(Request $request) {
        $types = $request->types;
        $jobs = \App\Job::with("jobtype")->with("city")->with("company")->where("status", "!=", 0)->whereIn("job_type_id", $types)->get();
        echo $view = view("front.jobs.render", compact("jobs"))->render();
        die();
    }

}
