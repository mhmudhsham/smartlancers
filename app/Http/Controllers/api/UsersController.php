<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Session;
use App\Jobseeker;

class UsersController extends Controller {

    public function login(Request $request) {        
        $validator = \Validator::make($request->all(), [
            'email' => 'required',
            "password" => "required"
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Login Failed', $validator->messages()->toJson());            
        }

        $email = strip_tags($request->email);
        $password = md5($request->password);
        $jobseeker = Jobseeker::where("email", $email)->where("password", $password)->first();                        
        if($jobseeker) {            
            $token = time() . "$" . md5(date("d - m - Y h : i : s") . "mhmudhsham");
            Session::put("token", $token);
            Session::put("token_user_data", $jobseeker);
            Session::save();            
            return $this->result(1, 'Login Successfully ', $token, $jobseeker);            
        } else {
            return $this->result(0, 'Login Failed', '', array());
        }
    }



    public function register(Request $request) {
        $validator = \Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required|unique:jobseekers',
            "password" => "required",
            'phone' => 'required|regex:/(01)[0-9]{9}/'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Registeration Failed', '', $validator->messages()->toJson());            
        }
        $username = strip_tags($request->username);
        $password = md5($request->password);
        $email = strip_tags($request->email);
        $phone = strip_tags($request->phone); 

        $jobseeker = new \App\Jobseeker;
        $jobseeker->username = $username;
        $jobseeker->password = $password;
        $jobseeker->email = $email;
        $jobseeker->phone = $phone;
        $id = $jobseeker->save();

        if($id) {
            $token = time() . "$" . md5(date("d - m - Y h : i : s") . "mhmudhsham");
            Session::put("token", $token);
            Session::put("token_user_data", $jobseeker);
            Session::save();            
            return $this->result(1, 'Registeration Successfully ', $token, $jobseeker);            
        } else {
            return $this->result(0, 'Registeration Failed', '', array());
        }
    }



    public function subscribe(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required',
            'sub_email' => 'required|unique:subscribers'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Subscribe Failed', '', $validator->messages()->toJson());            
        }

        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $subscribe = new \App\Subscribe;
        $subscribe->sub_email = $request->sub_email;
        $subscribe->dateadded = time();
        $subscribe->save(); 

        return $this->result(1, 'Subscribed Successfully ', $api_token, $subscribe);            
    }



    public function profile(Request $request) {
        
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->result(0, 'Unknown Token ... ', '', $validator->messages()->toJson());            
        }

        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $token_user_data = \Session::get("token_user_data");      
        $user_id = $token_user_data->id;
        $data = \App\Jobseeker::find($user_id);

        return $this->result(1, "Profile Data", $api_token, $data);
    }

    public function edit_profile(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->result(0, 'Unknown Token ... ', '', $validator->messages()->toJson());            
        }

        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }


    }


    public function change_password(Request $request) {
        $validator = \Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required',
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->result(0, 'Operation Failed ... ', '', $validator->messages()->toJson());            
        }

        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $token_user_data = \Session::get("token_user_data");      
        $user_id = $token_user_data->id;
 
        $data = \App\Jobseeker::find($user_id);
        $old_password = md5($request->old_password);
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;
        if ($data->password != $old_password) {
            return $this->result(0, 'Old password not true ', $api_token, []);            
        }

        if ($new_password != $confirm_password) {
                return $this->result(0, 'Passwords not equal ', $api_token, []);            
        }
        $data->password = md5($request->new_password);
        $data->save();

        return $this->result(0, 'Passwords updated successfully ...  ', $api_token, []);            
    }


 

    function result($status, $message, $token, $data)
    {
        $response = Response::json(array('status' => $status, 'message' => $message, 'token' => $token, 'data' => $data));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function index() {
        return get_name();
    }
}
