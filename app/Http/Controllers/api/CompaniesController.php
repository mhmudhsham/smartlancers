<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\Job;
use Session;

class CompaniesController extends Controller {
  
    public function index(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $companies = \App\Company::with("city")->withCount("jobs")->where("status", "!=", 0)->get();
        return $this->result(1, 'Companies', $api_token, $companies);   
    }

    public function details(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required',
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }
        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $id = $request->id;
        $data['jobs'] = \App\Job::with("jobtype")->where("com_id", $id)->get();
        $data['details'] = \App\Company::find($id);
        return $this->result(1, 'Company Details', $api_token, $data);            
    }

    function result($status, $message, $token, $data) {
        $response = Response::json(array('status' => $status, 'message' => $message, 'token' => $token, 'data' => $data));
        $response->header('Content-Type', 'application/json');
        return $response;
    }


}

?>