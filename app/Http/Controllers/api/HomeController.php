<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Session;

class HomeController extends Controller {
  
    public function aboutus(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }
        $settings = \App\Setting::all();
        $_settings = $this->loadSettings($settings); 
        $data = [];
        $data['aboutus_en'] = $_settings->aboutus_en;
        $data['aboutus_ar'] = $_settings->aboutus_ar;        
        return $this->result(1, 'About us', $api_token, $data);   
    }

    public function loadSettings($settings) {
        $settings_array = [];
        foreach($settings as $key => $value) {
            $settings_array[$value->title] = $value->content;
        }
        return (object) $settings_array;
    }

    function result($status, $message, $token, $data) {
        $response = Response::json(array('status' => $status, 'message' => $message, 'token' => $token, 'data' => $data));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function faq(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }
        $faq = \App\FAQ::orderBy("id", "desc")->get();
        return $this->result(1, 'FAQ', $api_token, $faq);   
    }

    public function contactus(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required',
            'full_name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $message = new \App\Message;
        $message->full_name = $request->full_name;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $result = $message->save();
        return $this->result(1, 'Message Sent Successfully', $api_token, $message);   
    }

}