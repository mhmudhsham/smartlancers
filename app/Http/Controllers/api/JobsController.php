<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use App\Job;
use Session;

class JobsController extends Controller {

    public function index(Request $request) {      
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }
        $api_token = $request->token;   
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $job_title = $request->job_title;
        $location  = $request->city;
        if($request->job_title != "" && $request->city != "") {
            $jobs = \App\Job::with("jobtype")
                ->with("city")
                ->with("company")
                ->where("status", "!=", 0)
                ->where("job_title", "LIKE", "%$job_title%")
                ->where("job_location", $location)
                ->get();
        }

        $jobs = Job::where("featured", 1)->get();
        return $this->result(1, 'Featured Jobs', $api_token, $jobs);            
    }

    public function cities(Request $request) {      
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }
        $api_token = $request->token;   
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $cities = \App\City::all(); 
        return $this->result(1, 'Cities', $api_token, $cities);            
    }


    public function categories(Request $request) {      
        $validator = \Validator::make($request->all(), [
            'token' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }
        $api_token = $request->token;   
        // $session_token = Session::get("token");
        // if($api_token != $session_token) {
        //     return $this->result(0, 'Unauthorized Token', '', []);   
        // }

        $cities = \App\Category::all(); 
        return $this->result(1, 'Categories', $api_token, $cities);            
    }

    public function details(Request $request) {
        $validator = \Validator::make($request->all(), [
            'token' => 'required',
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->result(0, 'Missed Token', '', $validator->messages()->toJson());            
        }
        
        $api_token = $request->token;        
        $session_token = Session::get("token");
        if($api_token != $session_token) {
            return $this->result(0, 'Unauthorized Token', '', []);   
        }

        $details = \App\Job::with("jobtype")->with('city')->with("company")->find($request->id);
        return $this->result(1, 'Job Details', $api_token, $details);            
    }


    




    
    function result($status, $message, $token, $data) {
        $response = Response::json(array('status' => $status, 'message' => $message, 'token' => $token, 'data' => $data));
        $response->header('Content-Type', 'application/json');
        return $response;
    }

}

?>