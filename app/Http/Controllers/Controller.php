<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Controller extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public $_lang;
    public $_slug;

    public function __construct() {
        $this->_lang = $currentLocale = app('laravellocalization')->getCurrentLocale();
        $this->loadSlug();
        view()->share("lang", $currentLocale);
        view()->share("slug", $this->_slug);
    }

    
    public function loadSlug() {
        $data = array();
        $data['question'] = "question_" . $this->_lang;
        $data['answer'] = "answer_" . $this->_lang;
        $data['title'] = "title_" . $this->_lang;
        $data['description'] = "description_" . $this->_lang;
        $slug = (object) $data;
        $this->_slug = $slug;
    }

}
