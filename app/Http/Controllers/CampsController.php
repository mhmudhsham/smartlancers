<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Camp;

class CampsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function add_camp() { 
        return view("front.camps.add");
    }

    public function store_camp(Request $request) {        
        $validator = Validator::make($request->all(), [
            'title_ar' => 'required|max:255',
            'title_en' => 'required|max:255',
            'description_ar' => "required",
            "description_en" => "required",
            "start_date" => "required",
            "end_date" => "required",
            "price" => "required",
            'logo' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect('camps/add')
                            ->withErrors($validator)
                            ->withInput();
        }


        $image = $request->file('logo');
        $logo = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/camps');
        $image->move($destinationPath, $logo);

        $course = new Camp;
        $course->logo = $logo;
        $course->title_ar = $request->title_ar;
        $course->title_en = $request->title_en;
        $course->description_ar = $request->description_ar;
        $course->description_en = $request->description_en;        
        $course->price = $request->price;
        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->is_active = 0;
        $course->save();
        
        return redirect('camps/add')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
        
    }    

    public function index() {
        $camps = Camp::orderBy("id", "desc")->where("is_active", 0)->paginate(16);            
        return view('front.camps.index', compact('camps'));
    }

    public function details($id) {    
        $details = Camp::find($id);
        return view("front.camps.view", compact('details'));
    }


    public function book(Request $request) {
        $camp_id = $request->camp_id;
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => "required",
            "message" => "required",           
        ]);
        if ($validator->fails()) {
            return redirect($this->_lang.'/camps/details/'.$camp_id)
                            ->withErrors($validator)
                            ->withInput();
        } 

        $course = new \App\CampReservation;       
        $course->name = $request->name;
        $course->camp_id = $request->camp_id;
        $course->email = $request->email;
        $course->phone = $request->phone;
        $course->message = $request->message;
        $course->save();

        return redirect()->back()->with('success', 'Your request sent successfully');    
    }
}
