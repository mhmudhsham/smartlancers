<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Category::orderBy("id", "desc")->paginate(100);
        return view("admin.categories.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Category::where("id", $id)->update(array("status" => $new_value));
        return $update;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.categories.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:categories|max:255',
                    'title_en' => 'required|unique:categories|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/categories')
                            ->withErrors($validator)
                            ->withInput();
        }
        $category = new Category;
        $category->title_ar = $request->title_ar;
        $category->title_en = $request->title_en;
        $category->status = $request->status;
        $category->save();
        return redirect('admin/categories')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Category::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.categories.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $category = Category::find($id);
        $category->title_ar = $request->title_ar;
        $category->title_en = $request->title_en;
        $category->status = $request->status;
        $category->save();
        return redirect('admin/categories')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $category = Category::find($id);
        $category->delete();
    }

}
