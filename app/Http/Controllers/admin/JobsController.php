<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Job;
use Illuminate\Http\Request;

class JobsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Job::orderBy("id", "desc")->paginate(100);
        return view("admin.jobs.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Job::where("id", $id)->update(array("status" => $new_value));
        return $update;
    }

    public function change_featured($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Job::where("id", $id)->update(array("featured" => $new_value));
        return $update;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
