<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\City;
use Illuminate\Http\Request;

class CitiesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = City::orderBy("id", "desc")->paginate(100);
        return view("admin.cities.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.cities.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:cities|max:255',
                    'title_en' => 'required|unique:cities|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/cities')
                            ->withErrors($validator)
                            ->withInput();
        }
        $city = new City;
        $city->title_ar = $request->title_ar;
        $city->title_en = $request->title_en;
        $city->save();
        return redirect('admin/cities')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = City::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.cities.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $city = City::find($id);
        $city->title_ar = $request->title_ar;
        $city->title_en = $request->title_en;
        $city->save();
        return redirect('admin/cities')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $city = City::find($id);
        $city->delete();
    }

}
