<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Material;

class MaterialsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $rows = Material::with("instructor")->orderBy("id", "desc")->paginate(100);
        return view("admin.materials.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Material::where("id", $id)->update(array("is_active" => $new_value));
        return $update;
    }

    public function show($id) {
        $details = Material::find($id);
        return view("admin.materials.view", compact("details"));
    }

}
