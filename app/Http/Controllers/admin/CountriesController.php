<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Country;
use Illuminate\Http\Request;

class CountriesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Country::orderBy("id", "desc")->paginate(100);
        return view("admin.countries.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.countries.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:countries|max:255',
                    'title_en' => 'required|unique:countries|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/countries')
                            ->withErrors($validator)
                            ->withInput();
        }
        $country = new Country;
        $country->title_ar = $request->title_ar;
        $country->title_en = $request->title_en;
        $country->save();
        return redirect('admin/countries')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Country::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.countries.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $country = Country::find($id);
        $country->title_ar = $request->title_ar;
        $country->title_en = $request->title_en;
        $country->save();
        return redirect('admin/countries')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $country = Country::find($id);
        $country->delete();
    }

}
