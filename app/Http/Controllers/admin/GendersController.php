<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Gender;
use Illuminate\Http\Request;

class GendersController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Gender::orderBy("id", "desc")->paginate(100);
        return view("admin.genders.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.genders.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:genders|max:255',
                    'title_en' => 'required|unique:genders|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/genders')
                            ->withErrors($validator)
                            ->withInput();
        }
        $gender = new Gender;
        $gender->title_ar = $request->title_ar;
        $gender->title_en = $request->title_en;
        $gender->save();
        return redirect('admin/genders')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Gender::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.genders.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $gender = Gender::find($id);
        $gender->title_ar = $request->title_ar;
        $gender->title_en = $request->title_en;
        $gender->save();
        return redirect('admin/genders')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $gender = Gender::find($id);
        $gender->delete();
    }

}
