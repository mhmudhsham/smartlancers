<?php

namespace App\Http\Controllers\admin;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // today statistics
        $dayData = array();
        $current_day_start = strtotime(date('Y-m-d 00:00:00'));
        $current_day_end = strtotime(date('Y-m-d 23:59:59'));

        $dayData['daySales'] = DB::table("orders")
                ->where("dateadded", ">=", $current_day_start)
                ->where("dateadded", "<", $current_day_end)
                ->sum("order_amount");

        $dayData['dayJobseekers'] = \App\Jobseeker::where("dateadded", ">=", $current_day_start)
                        ->where("dateadded", "<", $current_day_end)->count();

        $dayData['dayCompanies'] = \App\Company::where("dateadded", ">=", $current_day_start)
                        ->where("dateadded", "<", $current_day_end)->count();

        $dayData['dayJobs'] = \App\Job::where("dateadded", ">=", $current_day_start)
                        ->where("dateadded", "<", $current_day_end)->count();

        // week statistics
        $weekData = array();
        $week_first = strtotime(date("Y-m-d 00:00:00", strtotime('monday this week')));
        $week_today = time();

        $weekData['weekSales'] = DB::table("orders")
                ->where("dateadded", ">=", $week_first)
                ->where("dateadded", "<", $week_today)
                ->sum("order_amount");

        $weekData['weekJobseekers'] = \App\Jobseeker::where("dateadded", ">=", $week_first)
                        ->where("dateadded", "<", $week_today)->count();

        $weekData['weekCompanies'] = \App\Company::where("dateadded", ">=", $week_first)
                        ->where("dateadded", "<", $week_today)->count();

        $weekData['weekJobs'] = \App\Job::where("dateadded", ">=", $week_first)
                        ->where("dateadded", "<", $week_today)->count();

        // month statistics
        $monthData = array();
        $first_day_this_month = strtotime(date('Y-m-d 00:00:00', strtotime('first day of this month')));     // hard-coded '01' for first day
        $last_day_this_month = time();

        $monthData['monthSales'] = DB::table("orders")
                ->where("dateadded", ">=", $first_day_this_month)
                ->where("dateadded", "<", $last_day_this_month)
                ->sum("order_amount");

        $monthData['monthJobseekers'] = \App\Jobseeker::where("dateadded", ">=", $first_day_this_month)
                        ->where("dateadded", "<", $last_day_this_month)->count();

        $monthData['monthCompanies'] = \App\Company::where("dateadded", ">=", $first_day_this_month)
                        ->where("dateadded", "<", $last_day_this_month)->count();

        $monthData['monthJobs'] = \App\Job::where("dateadded", ">=", $first_day_this_month)
                        ->where("dateadded", "<", $last_day_this_month)->count();


        return view("admin.dashboard", compact('dayData', 'weekData', 'monthData'));
    }

    public function sendd() {
        $data['name'] = "Mahmoud";
        \Mail::send('emails.welcome', $data, function ($message) {
            $message->from('mhmudhsham8@gmail.com', 'Laravel');

            $message->to('mhmudhsham8@gmail.com');
        });
    }

}
