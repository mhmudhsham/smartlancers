<?php

namespace App\Http\Controllers\admin;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Account;

class AccountsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $rows = Account::orderBy("id", "desc")->paginate(100);
        return view("admin.accounts.index")->with(["rows" => $rows]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.accounts.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
                    'username' => 'required|max:150',
                    'email' => 'required|unique:users|max:150',
                    'password' => 'required|max:150'
        ]);
        if ($validator->fails()) {
            return redirect('admin/accounts')
                            ->withErrors($validator)
                            ->withInput();
        }
        $account = new Account;
        $account->name = $request->username;
        $account->email = $request->email;
        $account->password = bcrypt($request->password);
        $account->save();
        return redirect('admin/accounts')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $account = Account::find($id);
        $account->delete();
    }

}
