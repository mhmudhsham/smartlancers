<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $settings = Setting::pluck("content", "title");
        return view("admin.settings", compact("settings"));
    }

    public function store(Request $request) {
        foreach ($request->all() as $key => $one) {
            Setting::where("title", $key)->update(["content" => $one]);
        }
        return redirect('admin/');
    }

}
