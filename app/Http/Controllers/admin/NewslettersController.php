<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Newsletter;
use Illuminate\Http\Request;

class NewslettersController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Newsletter::orderBy("id", "desc")->paginate(100);
        return view("admin.newsletters.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.newsletters.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:newsletters|max:255',
                    'title_en' => 'required|unique:newsletters|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/newsletters')
                            ->withErrors($validator)
                            ->withInput();
        }
        $newsletter = new Newsletter;
        $newsletter->title_ar = $request->title_ar;
        $newsletter->title_en = $request->title_en;
        $newsletter->body_ar = $request->body_ar;
        $newsletter->body_en = $request->body_en;
        $newsletter->email = $request->email;
        $newsletter->month = $request->month;
        $newsletter->save();
        return redirect('admin/newsletters')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Newsletter::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.newsletters.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $newsletter = Newsletter::find($id);
        $newsletter->title_ar = $request->title_ar;
        $newsletter->title_en = $request->title_en;
        $newsletter->body_ar = $request->body_ar;
        $newsletter->body_en = $request->body_en;
        $newsletter->email = $request->email;
        $newsletter->month = $request->month;
        $newsletter->save();
        return redirect('admin/newsletters')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $newsletter = Newsletter::find($id);
        $newsletter->delete();
    }

}
