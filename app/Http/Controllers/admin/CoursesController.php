<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Course;

class CoursesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Course::with("instructor")->orderBy("id", "desc")->paginate(100);
        return view("admin.courses.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Course::where("id", $id)->update(array("is_active" => $new_value));
        return $update;
    }

    public function change_featured($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Course::where("id", $id)->update(array("is_featured" => $new_value));
        return $update;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $details = Course::find($id);
        return view("admin.courses.view", compact("details"));
    }

}
