<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Jobtype;
use Illuminate\Http\Request;

class JobtypesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Jobtype::orderBy("id", "desc")->paginate(100);
        return view("admin.jobtypes.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.jobtypes.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:jobtypes|max:255',
                    'title_en' => 'required|unique:jobtypes|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/jobtypes')
                            ->withErrors($validator)
                            ->withInput();
        }
        $jobtype = new Jobtype;
        $jobtype->title_ar = $request->title_ar;
        $jobtype->title_en = $request->title_en;
        $jobtype->save();
        return redirect('admin/jobtypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Jobtype::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.jobtypes.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $jobtype = Jobtype::find($id);
        $jobtype->title_ar = $request->title_ar;
        $jobtype->title_en = $request->title_en;
        $jobtype->save();
        return redirect('admin/jobtypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $jobtype = Jobtype::find($id);
        $jobtype->delete();
    }

}
