<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Company;
use Validator;
use Illuminate\Http\Request;

class CompaniesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Company::orderBy("id", "desc")->paginate(100);
        return view("admin.companies.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Company::where("id", $id)->update(array("status" => $new_value));
        return $update;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        $companytypes = \App\Companytype::all();
        $companyemployees = \App\Companyemployee::all();
        $pricing = \App\Pricing::all();
        return view("admin.companies.form", compact("id", "companytypes", "companyemployees", "pricing"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:companies|max:150',
                    'title_en' => 'required|unique:companies|max:150',
                    'user_fullname' => 'required|max:150',
                    'user_email' => 'required|max:150',
                    'user_password' => 'required|max:150',
                    'priceplan_id' => 'required|max:150',
                    'package_expirydate' => 'required|max:150',
                    'description' => 'required',
                    'phone_number' => 'required|max:150',
                    'email' => 'required|max:150',
                    'website' => 'required',
                    'companyemployee_id' => 'required',
                    'companytype_id' => 'required',
                    'the_order' => 'required',
                    'image' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/companies')
                            ->withErrors($validator)
                            ->withInput();
        }

        $image = $request->file('logo');
        $logo = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/companies');
        $image->move($destinationPath, $logo);


        $company = new Company;
        $company->logo = $logo;
        $company->title_ar = $request->title_ar;
        $company->title_en = $request->title_en;
        $company->user_fullname = $request->user_fullname;
        $company->user_email = $request->user_email;
        $company->user_password = $request->user_password;
        $company->priceplan_id = $request->priceplan_id;
        $company->package_expirydate = $request->package_expirydate;
        $company->description = $request->description;
        $company->phone_number = $request->phone_number;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->status = $request->status;
        $company->companyemployee_id = $request->companyemployee_id;
        $company->companytype_id = $request->companytype_id;
        $company->the_order = $request->the_order;

        $company->save();
        return redirect('admin/companies')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Company::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $companytypes = \App\Companytype::all();
        $companyemployees = \App\Companyemployee::all();
        $pricing = \App\Pricing::all();
        return view("admin.companies.form", compact("id", "companytypes", "companyemployees", "pricing"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (isset($request->logo) && $request->logo != "") {
            $image = $request->file('logo');
            $logo = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/companies');
            $image->move($destinationPath, $logo);
        }
        $company = Company::find($id);
        $company->title_ar = $request->title_ar;
        if (isset($request->logo) && $request->logo != "") {
            $company->logo = $logo;
        }
        $company->title_en = $request->title_en;
        $company->user_fullname = $request->user_fullname;
        $company->user_email = $request->user_email;
        $company->user_password = $request->user_password;
        $company->priceplan_id = $request->priceplan_id;
        $company->package_expirydate = $request->package_expirydate;
        $company->description = $request->description;
        $company->phone_number = $request->phone_number;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->status = $request->status;
        $company->companyemployee_id = $request->companyemployee_id;
        $company->companytype_id = $request->companytype_id;
        $company->the_order = $request->the_order;
        $company->save();
        return redirect('admin/companies')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $company = Company::find($id);
        $company->delete();
    }

}
