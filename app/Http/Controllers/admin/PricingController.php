<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Pricing;
use Validator;
use Illuminate\Http\Request;

class PricingController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Pricing::orderBy("id", "desc")->paginate(100);
        return view("admin.pricing.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.pricing.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:pricing_plan|max:150',
                    'title_en' => 'required|unique:pricing_plan|max:150',
                    'price' => 'required|max:150',
                    'job_post_number' => 'required|max:150',
                    'featured_job_post_number' => 'required|max:150',
                    'days_listing_duration' => 'required|max:150',
        ]);

        if ($validator->fails()) {
            return redirect('admin/pricing')
                            ->withErrors($validator)
                            ->withInput();
        }


        $pricing = new Pricing;
        $pricing->title_ar = $request->title_ar;
        $pricing->title_en = $request->title_en;
        $pricing->price = $request->price;
        $pricing->job_post_number = $request->job_post_number;
        $pricing->featured_job_post_number = $request->featured_job_post_number;
        $pricing->days_listing_duration = $request->days_listing_duration;
        $pricing->save();
        return redirect('admin/pricing')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Pricing::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.pricing.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $pricing = Pricing::find($id);
        $pricing->title_ar = $request->title_ar;
        $pricing->title_en = $request->title_en;
        $pricing->price = $request->price;
        $pricing->job_post_number = $request->job_post_number;
        $pricing->featured_job_post_number = $request->featured_job_post_number;
        $pricing->days_listing_duration = $request->days_listing_duration;
        $pricing->save();
        return redirect('admin/pricing')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $pricing = Pricing::find($id);
        $pricing->delete();
    }

}
