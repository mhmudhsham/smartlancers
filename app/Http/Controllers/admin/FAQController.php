<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\FAQ;
use Illuminate\Http\Request;

class FAQController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = FAQ::orderBy("id", "desc")->paginate(100);
        return view("admin.faq.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        $types = \App\Faqtype::all();
        return view("admin.faq.form", compact("id", "types"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'question_ar' => 'required',
                    'question_en' => 'required',
                    'answer_ar' => 'required',
                    'answer_en' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/faq')
                            ->withErrors($validator)
                            ->withInput();
        }

        $faq = new FAQ;
        $faq->question_ar = $request->question_ar;
        $faq->question_en = $request->question_en;
        $faq->answer_ar = $request->answer_ar;
        $faq->answer_en = $request->answer_en;
        $faq->faqtype_id = $request->faqtype_id;
        $faq->save();
        return redirect('admin/faq')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = FAQ::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $types = \App\Faqtype::all();
        return view("admin.faq.form", compact("id", "types"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $faq = FAQ::find($id);
        $faq->question_ar = $request->question_ar;
        $faq->question_en = $request->question_en;
        $faq->answer_ar = $request->answer_ar;
        $faq->answer_en = $request->answer_en;
        $faq->faqtype_id = $request->faqtype_id;
        $faq->save();
        return redirect('admin/faq')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $faq = FAQ::find($id);
        $faq->delete();
    }

}
