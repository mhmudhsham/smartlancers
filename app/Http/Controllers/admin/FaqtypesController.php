<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Faqtype;
use Validator;
use Illuminate\Http\Request;

class FaqtypesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Faqtype::orderBy("id", "desc")->paginate(100);
        return view("admin.faqtypes.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.faqtypes.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:faqtypes|max:255',
                    'title_en' => 'required|unique:faqtypes|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/faqtypes')
                            ->withErrors($validator)
                            ->withInput();
        }
        $faqtype = new Faqtype;
        $faqtype->title_ar = $request->title_ar;
        $faqtype->title_en = $request->title_en;
        $faqtype->save();
        return redirect('admin/faqtypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Faqtype::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.faqtypes.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $faqtype = Faqtype::find($id);
        $faqtype->title_ar = $request->title_ar;
        $faqtype->title_en = $request->title_en;
        $faqtype->save();
        return redirect('admin/faqtypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $faqtype = Faqtype::find($id);
        $faqtype->delete();
    }

}
