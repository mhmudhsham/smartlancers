<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Camp;

class CampsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Camp::orderBy("id", "desc")->paginate(100);
        return view("admin.camps.index", compact("rows"));
    }

    public function change_state($value, $id) {
        $new_value = 0;
        if ($value == 0) {
            $new_value = 1;
        }
        $update = Camp::where("id", $id)->update(array("is_active" => $new_value));
        return $update;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $details = Camp::find($id);
        return view("admin.Camps.view", compact("details"));
    }

    public function camp_reservations() {
        $rows = \App\CampReservation::paginate(100);
        return view("admin.camps.reservations", compact("rows"));
    }
}
