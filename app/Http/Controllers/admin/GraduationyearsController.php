<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Graduationyear;
use Illuminate\Http\Request;

class GraduationyearsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Graduationyear::orderBy("id", "desc")->paginate(100);
        return view("admin.graduationyears.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.graduationyears.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'year' => 'required|unique:graduationyears|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/graduationyears')
                            ->withErrors($validator)
                            ->withInput();
        }
        $graduationyear = new Graduationyear;
        $graduationyear->year = $request->year;
        $graduationyear->save();
        return redirect('admin/graduationyears')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Graduationyear::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.graduationyears.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $graduationyear = Graduationyear::find($id);
        $graduationyear->year = $request->year;
        $graduationyear->save();
        return redirect('admin/graduationyears')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $graduationyear = Graduationyear::find($id);
        $graduationyear->delete();
    }

}
