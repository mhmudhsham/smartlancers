<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Companytype;
use Illuminate\Http\Request;

class CompanytypesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Companytype::orderBy("id", "desc")->paginate(100);
        return view("admin.companytypes.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.companytypes.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:companytypes|max:255',
                    'title_en' => 'required|unique:companytypes|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/companytypes')
                            ->withErrors($validator)
                            ->withInput();
        }
        $companytype = new Companytype;
        $companytype->title_ar = $request->title_ar;
        $companytype->title_en = $request->title_en;
        $companytype->save();
        return redirect('admin/companytypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Companytype::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.companytypes.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $companytype = Companytype::find($id);
        $companytype->title_ar = $request->title_ar;
        $companytype->title_en = $request->title_en;
        $companytype->save();
        return redirect('admin/companytypes')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $companytype = Companytype::find($id);
        $companytype->delete();
    }

}
