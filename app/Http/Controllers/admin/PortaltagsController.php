<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Portaltag;
use Illuminate\Http\Request;

class PortaltagsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Portaltag::orderBy("id", "desc")->paginate(100);
        return view("admin.portaltags.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.portaltags.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:portaltags|max:255',
                    'title_en' => 'required|unique:portaltags|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/portaltags')
                            ->withErrors($validator)
                            ->withInput();
        }
        $portaltag = new Portaltag;
        $portaltag->title_ar = $request->title_ar;
        $portaltag->title_en = $request->title_en;
        $portaltag->save();
        return redirect('admin/portaltags')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Portaltag::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.portaltags.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $portaltag = Portaltag::find($id);
        $portaltag->title_ar = $request->title_ar;
        $portaltag->title_en = $request->title_en;
        $portaltag->save();
        return redirect('admin/portaltags')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $portaltag = Portaltag::find($id);
        $portaltag->delete();
    }

}
