<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Instructor;
use Validator;
use Illuminate\Http\Request;

class InstructorsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Instructor::orderBy("id", "desc")->paginate(100);
        return view("admin.instructors.index", compact("rows"));
    }

    public function courses($instructor_id) {
        $rows = \App\Course::orderBy("id", "desc")->where("instructor_id", $instructor_id)->paginate(100);
        return view("admin.courses.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.instructors.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:150',
                    'email' => 'required|unique:instructors|max:150',
                    'username' => 'required|unique:instructors|max:150',
                    'password' => 'required|max:150',
        ]);

        if ($validator->fails()) {
            return redirect('admin/instructors')
                            ->withErrors($validator)
                            ->withInput();
        }

        if (isset($request->logo) && $request->logo != "") {
            $image = $request->file('logo');
            $logo = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/instructors');
            $image->move($destinationPath, $logo);
        }

        $instructor = new Instructor;
        if (isset($request->logo) && $request->logo != "") {
            $instructor->image = $logo;
        }
        $instructor->name = $request->name;
        $instructor->email = $request->email;
        $instructor->phone = $request->phone;
        $instructor->experience = $request->experience;
        $instructor->bio = $request->bio;
        $instructor->username = $request->username;
        $instructor->password = bcrypt($request->password);


        $instructor->save();
        return redirect('admin/instructors')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Instructor::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.instructors.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (isset($request->logo) && $request->logo != "") {
            $image = $request->file('logo');
            $logo = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/instructors');
            $image->move($destinationPath, $logo);
        }
        $instructor = Instructor::find($id);
        if (isset($request->logo) && $request->logo != "") {
            $instructor->image = $logo;
        }
        $instructor->name = $request->name;
        $instructor->email = $request->email;
        $instructor->phone = $request->phone;
        $instructor->experience = $request->experience;
        $instructor->bio = $request->bio;
        $instructor->username = $request->username;
        if (isset($request->password) && $request->password != "") {
            $instructor->password = bcrypt($request->password);
        }

        $instructor->save();
        return redirect('admin/instructors')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $instructor = Instructor::find($id);
        $instructor->delete();
    }

}
