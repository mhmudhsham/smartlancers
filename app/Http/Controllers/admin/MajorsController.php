<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Major;
use Validator;
use Illuminate\Http\Request;

class MajorsController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Major::orderBy("id", "desc")->paginate(100);
        return view("admin.majors.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.majors.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'title_ar' => 'required|unique:majors|max:255',
                    'title_en' => 'required|unique:majors|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/majors')
                            ->withErrors($validator)
                            ->withInput();
        }
        $Major = new Major;
        $Major->title_ar = $request->title_ar;
        $Major->title_en = $request->title_en;
        $Major->save();
        return redirect('admin/majors')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Major::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.majors.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $Major = Major::find($id);
        $Major->title_ar = $request->title_ar;
        $Major->title_en = $request->title_en;
        $Major->save();
        return redirect('admin/majors')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $Major = Major::find($id);
        $Major->delete();
    }

}
