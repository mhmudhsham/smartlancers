<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Companyemployee;
use Illuminate\Http\Request;

class CompanyemployeesController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $rows = Companyemployee::orderBy("id", "desc")->paginate(100);
        return view("admin.companyemployees.index", compact("rows"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $id = 0;
        return view("admin.companyemployees.form", compact("id"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
                    'employees_count' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return redirect('admin/companyemployees')
                            ->withErrors($validator)
                            ->withInput();
        }
        $companyemployee = new Companyemployee;
        $companyemployee->employees_count = $request->employees_count;
        $companyemployee->save();
        return redirect('admin/companyemployees')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = Companyemployee::find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        return view("admin.companyemployees.form", compact("id"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $companyemployee = Companyemployee::find($id);
        $companyemployee->employees_count = $request->employees_count;
        $companyemployee->save();
        return redirect('admin/companyemployees')->with(
                        'info', 'Permission has been added to your Collaborator successfully'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $companyemployee = Companyemployee::find($id);
        $companyemployee->delete();
    }

}
