<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

    public $table = "courses";

    public function jobs() {
        return $this->hasMany("App\Material", "course_id");
    }

    public function major() {
        return $this->belongsTo("App\Major", "major_id");
    }

    public function instructor() {
        return $this->belongsTo("App\Instructor", "instructor_id");
    }

}
