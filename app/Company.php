<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    public $table = "companies";

    public function jobs() {
        return $this->hasMany("App\Job", "com_id");
    }

    public function companytype() {
        return $this->belongsTo("App\Companytype", "companytype_id");
    }

    public function city() {
        return $this->belongsTo("App\City", 'city_id');
    }

}
