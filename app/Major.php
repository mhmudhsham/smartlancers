<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model {

    public $table = "majors";

    public function courses() {
        return $this->hasMany("App\Course", "major_id");
    }

}
