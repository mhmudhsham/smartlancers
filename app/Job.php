<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {

    public $table = "jobs";

    public function company() {
        return $this->belongsTo("App\Company", "com_id");
    }

    public function city() {
        return $this->belongsTo("App\City", "job_location");
    }

    public function jobtype() {
        return $this->belongsTo("App\Jobtype", "job_type_id");
    }

}
