<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampReservation extends Model {

    public $table = "camps_reservations";

    public function camp() {
        return $this->belongsTo("App\Camp", "camp_id");
    }

}
