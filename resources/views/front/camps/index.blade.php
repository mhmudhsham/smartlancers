@extends("layouts.front") 
@section("content")
<div class="theme-layout" id="scrollup">
	<section class="overlape">
		<div class="block no-padding">
		<div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ trans("lang.All Voluntary programs") }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="allcourses-page">
		<div class="block">
			<div class="container">
				<div class="row courses-items">



					@foreach($camps as $one)
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="course-item prog-item">
						<div class="row">
						<div class="col-xs-4"> 
							<a href="{{ url($lang.'/camps/details/'.$one->id) }}" class="course-img">
                                    <img src="{{ url('uploads/camps/'.$one->logo) }}" alt="{{ $one->{$slug->title} }}">
                            </a>
							</div>
							<div class="col-xs-8"> 
							<h5 class="course-title">{{ $one->{$slug->title} }}</h5>
							<div class="course-span">
								<i class="la la-clock-o"></i><span>{{ $one->start_date }}</span>
								<!-- <i class="la la-users"></i><span>120</span> -->
							</div>
							<h5 class="course-salary">{{$one->price}} S.R.</h5>
							<a href="{{ url('/camps/details/'.$one->id) }}" class="blue-btn">{{ trans("lang.subscribe now") }}</a>
							</div>
							</div>
						</div>
					</div>
					@endforeach 


					
					
					
					
					
					
					
				</div>
				{{$camps->links()}}
				<!-- <div class="pagination">
					<ul>
						<li class="prev"><a href=""><i class="la la-long-arrow-left"></i> Prev</a></li>
						<li><a href="">1</a></li>
						<li class="active"><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><span class="delimeter">...</span></li>
						<li><a href="">14</a></li>
						<li class="next"><a href="">Next <i class="la la-long-arrow-right"></i></a></li>
					</ul>
				</div> -->
			</div>
		</div>
	</section>

	
</div>










@stop