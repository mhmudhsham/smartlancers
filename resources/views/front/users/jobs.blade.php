@extends("layouts.front")
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.Welcome To Your Profile") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column border-right">
                        <div class="widget">
                            <div class="tree_widget-sec">
                                <ul>
                                    <li class="">
                                        <a href="{{ url($lang."/profile") }}" title=""><i class="la la-file-text"></i>{{ trans("lang.My Profile") }}</a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url($lang."/client_jobs") }}" title=""><i class="la la-briefcase"></i>{{ trans("lang.My Jobs") }}</a>
                                    </li>
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-paper-plane"></i>Resumes</a>
                                                                        </li>-->
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-file-text"></i>Post a New Job</a>
                                                                        </li>-->
                                    <li class="">
                                        <a href="{{ url($lang."/change_password") }}" title=""><i class="la la-lock"></i>{{ trans("lang.Change Password") }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url($lang."/client_logout") }}" title=""><i class="la la-unlink"></i>{{ trans("lang.Logout") }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>

                    <div class="col-lg-9 column">
                        <div class="job-list-modern">
                            <div class="job-listings-sec no-border">



                                @forelse($jobs as $one)
                                <div class="job-listing wtabs">
                                    <div class="job-title-sec">
                                        <div class="c-logo">
                                            @php($image = \App\Company::find($one->job->com_id)->logo)
                                            <img src="{{ url('/uploads/companies/'.$image) }}" alt="" />
                                        </div>
                                        <h3>
                                            <a href="{{ url($lang.'/jobs/details/'.$one->job->id."/".str_replace(" ", "-", $one->job->job_title)) }}" title="{{ $one->job->job_title }}">{{ $one->job->job_title }}</a>
                                        </h3>
                                        <!--<span>Massimo Artemisis</span>-->
                                        @php($location = \App\City::find($one->job->job_location)->{$slug->title})
                                        <div class="job-lctn"><i class="la la-map-marker"></i>{{ $location }}</div>
                                    </div>
                                    <div class="job-style-bx">
                                        @php($type = \App\Jobtype::find($one->job->job_type_id)->{$slug->title})
                                        <span class="job-is ft">{{ $type }}</span>
                                        <!--<span class="fav-job"><i class="la la-heart-o"></i></span>-->
                                        <!--<i>5 months ago</i>-->
                                    </div>
                                </div>
                                @empty
                                <p>{{ trans("lang.No jobs") }}</p>
                                @endforelse


                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
</div>
</section>
</div>
@stop