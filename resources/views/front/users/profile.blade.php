@extends("layouts.front")
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.Welcome To Your Profile") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column border-right">
                        <div class="widget">
                            <div class="tree_widget-sec">
                                <ul>
                                    <li class="">
                                        <a href="{{ url($lang."/profile") }}" title=""><i class="la la-file-text"></i>{{ trans("lang.My Profile") }}</a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url($lang."/client_jobs") }}" title=""><i class="la la-briefcase"></i>{{ trans("lang.My Jobs") }}</a>
                                    </li>
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-paper-plane"></i>Resumes</a>
                                                                        </li>-->
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-file-text"></i>Post a New Job</a>
                                                                        </li>-->
                                    <li class="">
                                        <a href="{{ url($lang."/change_password") }}" title=""><i class="la la-lock"></i>{{ trans("lang.Change Password") }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url($lang."/client_logout") }}" title=""><i class="la la-unlink"></i>{{ trans("lang.Logout") }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                    <div class="col-lg-9 column">
                        <div class="padding-left">
                            <div class="profile-title" id="mp">
                                <h3>{{ trans("lang.My Profile") }}</h3>
                                @if($errors->any())
                                <p>{{$errors->first()}}</p>
                                @endif
                                <div class="upload-img-bar">
                                    <span><img src="{{ url('/uploads/jobseekers/'.$details->image) }}" alt="" /><i>x</i></span>
                                    <div class="upload-info">
                                        <form action="{{ url('/change_profile') }}" enctype="multipart/form-data" method="post">
                                            <!--                                            <a href="#" title="">Browse</a>-->
                                            {{ csrf_field() }}
                                            <input type="file" name="logo" />
                                            <input type="submit" value="{{ trans("lang.Save") }}" class="btn btn-info" />
                                        </form>
                                        <span>Max file size is 1MB, Minimum dimension: 270x210 And Suitable files are .jpg & .png</span>
                                    </div>
                                </div>
                            </div>
                            <div class="social-edit"  id="sn">
                                <h3>{{ trans("lang.Personal Data") }}</h3>
                                <form action="{{ url('/update_personal') }}">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.Name") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="name" />
                                                <!--<i class="fa fa-facebook"></i>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.Graduation Date") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="graduation_year" />
                                                <!--<i class="fa fa-twitter"></i>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.Phone") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="phone" />
                                                <!--<i class="la la-google"></i>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.Age") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="age" />
                                                <!--<i class="fa fa-linkedin"></i>-->
                                            </div>
                                        </div>
                                    </div>
                                    <!--</form>-->
                            </div>
                            <div class="contact-edit" id="ci">
                                <h3>{{ trans("lang.Other Data") }}</h3>
                                <!--<form>-->
                                <div class="row">
                                    <div class="col-lg-4">
                                        <span class="pf-title">{{ trans("lang.Email") }}</span>
                                        <div class="pf-field">
                                            <input type="email" name="email" placeholder="" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <span class="pf-title">{{ trans("lang.university") }}</span>
                                        <div class="pf-field">
                                            <select data-placeholder="{{ trans("lang.Select your university") }}" class="chosen" name="univ_id">
                                                @foreach($universities as $one)
                                                <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <span class="pf-title">{{ trans("lang.Country") }}</span>
                                        <div class="pf-field">
                                            <select name="country_id" data-placeholder="" class="chosen">
                                                @foreach($countries as $one)
                                                <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <span class="pf-title">{{ trans("lang.City") }}</span>
                                        <div class="pf-field">
                                            <select name="location" data-placeholder="" class="chosen">
                                                @foreach($cities as $one)
                                                <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button type="submit">{{ trans("lang.Update") }}</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop