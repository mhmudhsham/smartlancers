@extends("layouts.front")
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.Welcome To Your Profile") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column border-right">
                        <div class="widget">
                            <div class="tree_widget-sec">
                                <ul>
                                    <li class="">
                                        <a href="{{ url($lang."/profile") }}" title=""><i class="la la-file-text"></i>{{ trans("lang.My Profile") }}</a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url($lang."/client_jobs") }}" title=""><i class="la la-briefcase"></i>{{ trans("lang.My Jobs") }}</a>
                                    </li>
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-paper-plane"></i>Resumes</a>
                                                                        </li>-->
                                    <!--                                    <li class="inner-child">
                                                                            <a href="#" title=""><i class="la la-file-text"></i>Post a New Job</a>
                                                                        </li>-->
                                    <li class="">
                                        <a href="{{ url($lang."/change_password") }}" title=""><i class="la la-lock"></i>{{ trans("lang.Change Password") }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url($lang."/client_logout") }}" title=""><i class="la la-unlink"></i>{{ trans("lang.Logout") }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                    <div class="col-lg-9 column">
                        <div class="padding-left">
                            <div class="manage-jobs-sec">
                                <h3>{{ trans("lang.Change Password") }}</h3>
                                <div class="change-password">
                                    @if($errors->any())
                                    <p>{{$errors->first()}}</p>
                                    @endif
                                    <form action="{{ url('/store_password') }}" method="post" >
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <span class="pf-title">{{ trans("lang.Old Password") }}</span>
                                                <div class="pf-field">
                                                    <input type="password" name="old_password"  />
                                                </div>
                                                <span class="pf-title">{{ trans("lang.New Password") }}</span>
                                                <div class="pf-field">
                                                    <input type="password" name="new_password" />
                                                </div>
                                                <span class="pf-title">{{ trans("lang.Confirm Password") }}</span>
                                                <div class="pf-field">
                                                    <input type="password" name="confirm_password" />
                                                </div>
                                                <button type="submit">{{ trans("lang.Update") }}</button>
                                            </div>
                                            <div class="col-lg-6">
                                                <i class="la la-key big-icon"></i>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
@stop