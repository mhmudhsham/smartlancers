@extends("layouts.front")
@section("scripts")
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{!! url('assets/front/js/validation/contact_us.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.Contact Us") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block remove-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contact-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14948655.571317883!2d54.13456437209218!3d23.833738871533285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15e7b33fe7952a41%3A0x5960504bc21ab69b!2z2KfZhNiz2LnZiNiv2YrYqQ!5e0!3m2!1sar!2seg!4v1525001861919" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 column">
                        <div class="contact-form">
                            <h3>{{ trans("lang.Keep In Touch") }}</h3>
                            <form id="contact_us_form">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <span class="pf-title">{{ trans("lang.Full Name") }}</span>
                                        <div class="pf-field">
                                            <input class="required_field" name="full_name" type="text" placeholder="{{ trans("lang.Enter Your Name") }}" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <span class="pf-title">{{ trans("lang.Email") }}</span>
                                        <div class="pf-field">
                                            <input class="required_field" name="email" type="text" placeholder="{{ trans("lang.Enter Your Email") }}" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <span class="pf-title">{{ trans("lang.Subject") }}</span>
                                        <div class="pf-field">
                                            <input class="required_field" name="subject" type="text" placeholder="{{ trans("lang.Enter Subject") }}" />
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <span class="pf-title">{{ trans("lang.Leave Your Message") }}</span>
                                        <div class="pf-field">
                                            <textarea class="required_field" name="message"></textarea>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <button type="submit">{{ trans("lang.Send") }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6 column">
                        <div class="contact-textinfo style2">
                            <h3>JobHunt Office</h3>
                            <ul>
                                <li><i class="la la-map-marker"></i><span>Maka - saudi arabia.</span></li>
                                <li><i class="la la-phone"></i><span>Call Us : +966112222591</span></li>
                                <li><i class="la la-fax"></i><span>Fax : +966112222591</span></li>
                                <li><i class="la la-envelope-o"></i><span>Email : info@smartlancers.com</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop