@extends("layouts.front")
@section("content")

<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.FAQ") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="faqs">
                            @foreach($faq as $key => $one)
                            @if($key == 0)
                            <div class="faq-box">
                                <h2>{!! $one->{$slug->question} !!} <i class="la la-minus"></i></h2>
                                <div class="contentbox">
                                    <p>{!! $one->{$slug->answer} !!}</p>
                                </div>
                            </div>
                            @else
                            <div class="faq-box">
                                <h2 class="active">{!! $one->{$slug->question} !!} <i class="la la-minus"></i></h2>
                                <div class="contentbox" style="display: none;">
                                    <p>{!! $one->{$slug->answer} !!}</p>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop