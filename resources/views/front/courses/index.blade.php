@extends("layouts.front") 
@section("content")
<div class="theme-layout" id="scrollup">
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ trans("lang.All Courses") }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="allcourses-page">
		<div class="block">
			<div class="container">
				<div class="row courses-items">
                    @foreach($courses as $one)
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="course-item">
                                <a href="{{ url($lang.'/courses/details/'.$one->id) }}" class="course-img">
                                    <img src="{{ url('uploads/courses/'.$one->logo) }}" alt="{{ $one->{$slug->title} }}">
                                </a>
                                <h5 class="course-title">{{ $one->{$slug->title} }}</h5>
                                <div class="course-span">
                                    <i class="la la-clock-o"></i><span>{{ $one->start_date }}</span>
                                    <i class="la la-users"></i><span>{{$one->total_students}}</span>
                                </div>
                                <h5 class="course-salary">{{$one->price}} S.R.</h5>
                                <a href="{{ url('/courses/details/'.$one->id) }}" class="blue-btn">{{ trans("lang.subscribe now") }}</a>
                            </div>
                        </div>
                    @endforeach




                    


					
				</div>
				
				<div class="pagination">
                    {{$courses->links()}}
					<!-- <ul>
						<li class="prev"><a href=""><i class="la la-long-arrow-left"></i> Prev</a></li>
						<li><a href="">1</a></li>
						<li class="active"><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><span class="delimeter">...</span></li>
						<li><a href="">14</a></li>
						<li class="next"><a href="">Next <i class="la la-long-arrow-right"></i></a></li>
					</ul> -->
				</div>
			</div>
		</div>
	</section>
</div>
@stop