@extends("layouts.front") 
@section("content")
<div class="theme-layout" id="scrollup">
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ trans("lang.Add Course") }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="addcourse-page">
		<div class="block">
			<div class="container">

			<div class="row">
				
				@if(count($errors) > 0) 
				<ul>
				@foreach($errors as $one)
				<li class="alert alert-danger">{{$one}}</li>
				@endforeach
				</ul>
				@endif
					<div class="col-lg-12">
						<form class="subscribe-form" action='{{ url("/courses/store") }}' method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                
								<div class="form-group">
                                    <div class="col-md-6">
								  <label for="course">{{trans("lang.title_ar")}} :</label>
								  <input type="text" name='title_ar' class="form-control" id="" value="{{ old('title_ar') }}" placeholder="Enter Course title (AR)">
                                  </div>
                                  <div class="col-md-6">
								  <label for="course">{{trans("lang.title_en")}} :</label>
								  <input type="text" name='title_en' class="form-control" id="" value="{{ old('title_en') }}" placeholder="Enter Course title (EN)">
                                  </div>
								</div>


                                <div class="form-group">
                                    <div class="col-md-6">
								  <label for="course">{{trans("lang.description_ar")}} :</label>
                                  <textarea class="form-control" name="description_ar" ></textarea>
                                  </div>
                                  <div class="col-md-6">
								  <label for="course">{{trans("lang.description_en")}} :</label>
                                  <textarea class="form-control" name="description_en" ></textarea>
                                  </div>
								</div>

                                

                                <div class="form-group">
                                    <div class="col-md-6">
								  <label for="course">{{trans("lang.Total Hours")}} :</label>
								  <input type="number" class="form-control" name="total_hours" id="" placeholder="">
                                  </div>
                                  <div class="col-md-6">
								  <label for="course">{{trans("lang.Price")}} :</label>                                  
								  <input type="number" class="form-control" name="price" id="" placeholder="">
                                  </div>
								</div>



                                
                                <div class="form-group">
                                    <div class="col-md-6">
								  <label for="course">{{trans("lang.Cash Type")}} :</label>
								  <select name="cash_type"  class="form-control">    
                                        <option value="0">{{trans("lang.cash")}}</option>
								  		<option value="1">{{trans("lang.other")}}</option> 								  		
								  </select>
                                  </div>
                                  <div class="col-md-6">
								  <label for="course">{{trans("lang.Attendance Type")}} :</label>
                                  <select name="attendance_type" id=""   class="form-control"> 
								  		<option value="0">{{trans("lang.online")}}</option>
								  		<option value="1">{{trans("lang.place")}}</option>
								  </select>
                                  </div>
								</div>



								<div class="form-group">
                                    <div class="col-md-6">
								  <label for="course">{{trans("lang.Major")}} :</label>
								  <select name="major_id"  class="form-control">    
									@foreach($majors as $one)
										<option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>							  		
									@endforeach
								  </select>
                                  </div>
                                  <div class="col-md-6">
								  <label for="course">{{trans("lang.Session total hours")}} :</label>
                                  <input type="number" class="form-control" name="session_total_hours" />
                                  </div>
								</div>



								<div class="form-group">
								  <label for="start">{{trans("lang.Start Date")}} :</label>
								  <input type="date" name="start_date" class="form-control date" id="start" placeholder="Enter date">
								</div> 
								<div class="form-group">
								  <label for="upload">{{trans("lang.Course Main Image")}} :</label>
								  <input type="file" name="logo" class="form-control uploadfile file" id="upload" placeholder="Upload Image" data-preview-file-type="text">
								</div>

<!-- 

								<div class="form-group">
								  <label for="tea">Teacher :</label>
								  <select name="tea" id="tea"  class="form-control">
								  		<option value="professor" selected>mohamed</option>
								  		<option value="teacher">name here</option>
								  		<option value="teacher">teacher</option>
								  		<option value="teacher">teacher</option>
								  </select>
								</div>
								<div class="form-group">
								  <label for="cat">Category :</label>
								  <select name="cat" id="cat"  class="form-control">
								  		<option value="professor" selected>category selected</option>
								  		<option value="teacher">Skills</option>
								  		<option value="teacher">development</option>
								  </select>
								</div>





								<div class="form-group">
								  <label for="level">Course level :</label>
								  <select name="level" id="level"  class="form-control">
								  		<option value="professor" selected>beginner</option>
								  		<option value="teacher">medium</option>
								  		<option value="teacher">advanced</option>
								  		<option value="teacher">professional</option>
								  </select>
								</div>
								<div class="form-group">
								  <label for="start">Date start:</label>
								  <input type="date" class="form-control date" id="start" placeholder="Enter date">
								</div>
								<div class="form-group">
								  <label for="end">Date End:</label>
								  <input type="date" class="form-control date" id="end" placeholder="Enter date">
								</div>
								<div class="form-group">
								  <label for="upload">Course Main Image:</label>
								  <input type="file" class="form-control uploadfile file" id="upload" placeholder="Upload Image" data-preview-file-type="text">
								</div>
								<div class="form-group">
								  <label for="upload">Course Images:</label>
								  <input type="file" class="form-control uploadfile file" id="upload2" placeholder="Upload Image" data-preview-file-type="text" multiple>
								</div> -->
								<div class="clearfix"></div>
								<hr>
								<button type="submit" class="btn btn-default">Confirm</button>
						  </form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@stop