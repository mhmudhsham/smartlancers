@extends("layouts.front") 
@section("content")
<!-- Modal -->
<div id="myModal" class="modal fade subscribe-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Subscribe Form</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<!-- <div class="col-md-3 col-sm-3 col-xs-12 teacher-sub"> -->
        		<!-- <a href="#">
        			<img src="./assets/images/es1.jpg" alt="teacher image" class="img-thumbnail">
        		</a> -->
				<!-- <h6>Teacher Name</h6> -->
				<!-- <p class="text-muted">professional</p>
       			<a href="#" class="btn btn-primary btn-sm">profile</a> -->
        	<!-- </div> -->
        	<div class="col-md-9 col-sm-9 col-xs-12">
        		<form class="subscribe-form" action="{{ url('/courses/book') }}" method="post">
						{{csrf_field()}}
						<input type="hidden" name="course_id" value="{{ $details->id }}" />
					<div class="form-group">
					  <label for="first">{{ trans("lang.name") }} :</label>
					  <input type="text" class="form-control" id="" name="name" placeholder="{{ trans("lang.Enter your name") }}">
					</div>
					<div class="form-group">
					  <label for="last">{{ trans("lang.email") }} :</label>
					  <input type="text" class="form-control" id="" name="email" placeholder="{{ trans("lang.Enter your email") }}">
					</div>
					<div class="form-group">
					  <label for="last">{{ trans("lang.phone") }} :</label>
					  <input type="number" class="form-control" id="" name="phone" placeholder="{{ trans("lang.Enter your phone number") }}">
					</div>
					<div class="form-group">
					  <label for="email">{{ trans("lang.message") }} :</label>
					  <textarea  class="form-control" name="message" placeholder="{{ trans("lang.Enter your message") }}"></textarea>
					</div> 
					<div class="clearfix"></div>
					<hr>
					<button type="submit" class="btn btn-default">{{ trans("lang.Subscribe") }}</button>
			  </form>
        	</div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="theme-layout" id="scrollup">
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ trans("lang.Course Details") }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="course-details-page">
		<div class="block">
			<div class="container">
				<div class="row">
				 	<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
				 		<div class="job-single-info3">
						 @if (\Session::has('success'))
								<div class="alert alert-success">
										<ul>
												<li>{!! \Session::get('success') !!}</li>
										</ul>
								</div>
						@endif
							<h4><strong>{{ $details->{$slug->title} }}</strong></h4>
							<hr>
							<div class="text-muted">
                                {{ mb_substr($details->{$slug->description}, 0, 50) }}
							</div>
							<hr>
							<!-- <span><i class="la la-map-marker"></i>Sacramento, California</span> -->
							<span class="job-is ft">Online</span>
							<ul class="tags-jobs">
								<li><i class="la la-calendar-o"></i> {{trans("lang.Date")}} : {{  $details->start_date }}</li>
								<!-- <li><i class="la la-eye"></i> {{ trans("lang.Views") }} 5683</li> -->
								<li><i class="la la-users"></i>{{$details->total_students}} {{ trans("lang.students") }}</li>
							</ul>
							<div class="clearfix"></div>
							<hr>
							<ul class="unstyled-list">
								<li class="col-xs-6"><h2 class="course-details-salary">{{$details->price}} S.R</h2></li>
								<li class="col-xs-6">
								<a href="#" class="blue-btn-block" data-toggle="modal" data-target="#myModal">{{ trans("lang.subscribe now") }}</a></li>
							</ul>
						</div>
				 	</div>
				 	<div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
              <img width="100%" height="315" src="{{ url('uploads/courses/'.$details->logo) }}" alt="">				 		
				 	</div>
				</div>
				<div class="clearfix"></div>
			<div class="single-course-details">
				<div class=" ">
					<div class="col-lg-12">

						<ul class="nav nav-pills">
							<li class="active"><a data-toggle="pill" href="#home">{{ trans("lang.Details") }}</a></li>
							<!-- <li><a data-toggle="pill" href="#menu1">files</a></li>
							<li><a data-toggle="pill" href="#menu2">Menu 2</a></li>
							<li><a data-toggle="pill" href="#menu3">Menu 3</a></li> -->
						</ul>

						<div class="tab-content">
							
							<div id="home" class="tab-pane fade in active">
								<h3>{{ trans("lang.Details") }}</h3>
								<p>{{ $details->{$slug->description} }}</p>								
							</div>
							
							<div id="menu1" class="tab-pane fade">
								<h3>Files</h3>
								<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<ul>
									<li>Pdf file Ability to write code – <a href="#">DOWNLOAD</a></li>
									<li>Pdf file  write code – <a href="#">DOWNLOAD</a></li>
									<li>Pdf file Ability to – <a href="#">DOWNLOAD</a></li>
									<li>file Ability to write code – <a href="#">DOWNLOAD</a></li>
									
								</ul>
							</div>
							<div id="menu2" class="tab-pane fade">
								<h3>Menu 2</h3>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
							</div>
							<div id="menu3" class="tab-pane fade">
								<h3>Menu 3</h3>
								<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		
					</div>
				</div>
	</section>
</div>
@stop