@extends("layouts.front")
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.Welcome To Your Profile") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column border-right">
                        <div class="widget">
                            <div class="tree_widget-sec">
                                <ul>
                                    <li class="">
                                        <a href="{{ url($lang."/post_job") }}" title=""><i class="la la-file-text"></i>{{ trans("lang.Post a job") }}</a>
                                    </li>   
                                    <li class="">
                                        <a href="{{ url($lang."/company") }}" title=""><i class="la la-file-text"></i>{{ trans("lang.My Profile") }}</a>
                                    </li>                     
                                    <li class="">
                                        <a href="{{ url($lang."/change_password") }}" title=""><i class="la la-lock"></i>{{ trans("lang.Change Password") }}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url($lang."/company_logout") }}" title=""><i class="la la-unlink"></i>{{ trans("lang.Logout") }}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                    <div class="col-lg-9 column">
                        <div class="padding-left">
                            <div class="profile-title" id="mp">                               
                                @if($errors->any())
                                <p>{{$errors->first()}}</p>
                                @endif                            
                            </div>
                            <div class="social-edit"  id="sn">
                                <h3>{{ trans("lang.Course Details") }}</h3>
                                <form action="{{ url('/store_job') }}" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                    <div class="row">                                    
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.Job Title") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="job_title" /> 
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.salary") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="salary" /> 
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.working_hours") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="working_hours" /> 
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.category") }}</span>
                                            <div class="pf-field">
                                                <select name="category_id" id="">
                                                    @foreach($categories as $one)
                                                        <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        

                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.job_location") }}</span>
                                            <div class="pf-field">
                                                <input type="text" placeholder="" name="job_location" /> 
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.job_type") }}</span>
                                            <div class="pf-field">
                                                <select name="job_type_id" id="">
                                                    @foreach($jobtypes as $one)
                                                        <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.gender") }}</span>
                                            <div class="pf-field">
                                                <select name="gender" id="">
                                                    <option value="0">Male</option>
                                                    <option value="1">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <span class="pf-title">{{ trans("lang.short_desc") }}</span>
                                            <div class="pf-field">
                                                <textarea name="short_desc" id="" cols="30" ></textarea>
                                            </div>
                                        </div>
                                        

                                        <div class="col-lg-6" style="margin-bottom: 15px;">
                                            <button class="btn btn-info">{{ trans("lang.Save") }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop