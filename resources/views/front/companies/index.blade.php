@extends("layouts.front")
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ trans("lang.companies") }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block no-padding">
            <div class="container">
                <!--                <div class="row no-gape">
                                    <aside class="col-lg-4 column border-right">
                                        <div class="widget">
                                            <div class="search_widget_job">
                                                <div class="field_w_search">
                                                    <input type="text" placeholder="{{ trans("lang.Search Keywords") }}" />
                                                    <i class="la la-search"></i>
                                                </div> Search Widget
                                                <div class="field_w_search">
                                                    <input type="text" placeholder="{{ trans("lang.All Locations") }}" />
                                                    <i class="la la-map-marker"></i>
                                                </div> Search Widget
                                            </div>
                                        </div>
                                                                <div class="widget">
                                                                    <h3 class="sb-title open">Specialism</h3>
                                                                    <div class="specialism_widget">
                                                                        <div class="simple-checkbox">
                                                                            <p><input type="checkbox" name="spealism" id="as"><label for="as">Accountancy (2)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="asd"><label for="asd">Banking (2)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="errwe"><label for="errwe">Charity & Voluntary (3)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="fdg"><label for="fdg">Digital & Creative (4)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="sc"><label for="sc">Estate Agency (3)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="aw"><label for="aw">Graduate (2)</label></p>
                                                                            <p><input type="checkbox" name="spealism" id="ui"><label for="ui">IT Contractor (7)</label></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="widget">
                                                                <h3 class="sb-title open">Job</h3>
                                                                <div class="specialism_widget">
                                                                    <div class="simple-checkbox">
                                                                        <p><input type="checkbox" name="spealism" id="t1"><label for="t1">Full-Time</label></p>
                                                                        <p><input type="checkbox" name="spealism" id="t2"><label for="t2">Part-Time</label></p>
                                                                        <p><input type="checkbox" name="spealism" id="t3"><label for="t3">Per-Task</label></p>
                                                                        <p><input type="checkbox" name="spealism" id="t4"><label for="t4">Freelancer</label></p>
                                                                        <p><input type="checkbox" name="spealism" id="t5"><label for="t5">Once Job</label></p>
                                                                    </div>
                                                                </div>
                                </div>-->
                <!--</aside>-->
                <div class="col-lg-12 column">
                    <div class="emply-list-sec">


                        @if(count($companies) > 0)
                        @foreach($companies as $one)
                        <div class="emply-list">
                            <div class="emply-list-thumb">
                                <a href="{{ url($lang.'/companies/details/'.$one->id."/".str_replace(" ", "-", $one->{$slug->title})) }}" title="{{ $one->{$slug->title} }}">
                                    <img src="{{ url('uploads/companies/'.$one->logo) }}" alt="" />
                                </a>
                            </div>
                            <div class="emply-list-info">
                                <div class="emply-pstn">{{ $one->jobs_count }} {{ trans("lang.Open Positions") }}</div>
                                <h3><a href="{{ url($lang.'/companies/details/'.$one->id."/".str_replace(" ", "-", $one->{$slug->title})) }}" title="{{ $one->{$slug->title} }}">{{ $one->{$slug->title} }}</a></h3>
                                <!--<span>Accountancy, Human Resources</span>-->
                                <!--<h6><i class="la la-map-marker"></i> </h6>-->
                                <p> {{ mb_substr($one->description, 0, 200) }} </p>
                            </div>
                        </div><!-- Employe List -->
                        @endforeach
                        @endif



                        <div class="pagination">
                            <ul>
                                <li class="prev"><a href=""><i class="la la-long-arrow-left"></i> Prev</a></li>
                                <li><a href="">1</a></li>
                                <li class="active"><a href="">2</a></li>
                                <li><a href="">3</a></li>
                                <li><span class="delimeter">...</span></li>
                                <li><a href="">14</a></li>
                                <li class="next"><a href="">Next <i class="la la-long-arrow-right"></i></a></li>
                            </ul>
                        </div><!-- Pagination -->
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

</div>
@stop