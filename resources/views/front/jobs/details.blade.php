@extends("layouts.front")
@section("scripts")
<script src="{!! url('assets/front/js/validation/job.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header">
                            <h3>{{ $details->job_title }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 column">
                        <div class="job-single-sec">
                            <div class="job-single-head2">
                                <div class="job-title2"><h3>{{ $details->job_title }}</h3><span class="job-is ft">Full time</span><i class="la la-heart-o"></i></div>
                                <ul class="tags-jobs">
                                    <li><i class="la la-map-marker"></i> {{ $details->city->{$slug->title} }} </li>
                                    <li><i class="la la-money"></i> {{ trans("lang.Monthly Salary") }} : <span>{{ $details->salary }}</span></li>
                                    <li><i class="la la-calendar-o"></i> {{ trans("lang.Post Date") }} : {{ $details->created_at }}</li>
                                </ul>
                                <span><strong>{{ trans("lang.skills") }} </strong> : {{ $details->skills_required }}</span>
                            </div><!-- Job Head -->
                            <div class="job-details">
                                <h3>{{ trans("lang.Job Description") }}</h3>
                                <p> {{ $details->short_desc }} </p>
                            </div>
                            <div class="job-overview">
                                <h3>{{ trans("lang.Job Overview") }} </h3>
                                <ul>
                                    <li><i class="la la-money"></i><h3>{{ trans("lang.Offered Salary") }}</h3><span>{{ $details->salary }}</span></li>
                                    <!--<li><i class="la la-mars-double"></i><h3>Gender</h3><span>Female</span></li>-->
                                    <!--<li><i class="la la-thumb-tack"></i><h3>Career Level</h3><span>Executive</span></li>-->
                                    <li><i class="la la-puzzle-piece"></i><h3>{{ trans("lang.Type") }}</h3><span>{{ $details->jobtype->{$slug->title} }}</span></li>
                                    <li><i class="la la-shield"></i><h3>{{ trans("lang.Experience") }}</h3><span>{{ $details->experience_id }} </span></li>
                                    <!--<li><i class="la la-line-chart "></i><h3>Qualification</h3><span>Bachelor Degree</span></li>-->
                                </ul>
                            </div><!-- Job Overview -->
                            <!--                            <div class="share-bar">
                                                            <span>Share</span><a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a><a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
                                                        </div>-->
                        </div>
                    </div>
                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="job-single-head style2">
                            <!--<div class="job-thumb"> <img src="./assets/images/sjs.png" alt="" /> </div>-->
                            <!--                            <div class="job-head-info">
                                                            <h4>Tix Dog</h4>
                                                            <span>274 Seven Sisters Road, London, N4 2HY</span>
                                                            <p><i class="la la-unlink"></i> www.jobhunt.com</p>
                                                            <p><i class="la la-phone"></i> +90 538 963 54 87</p>
                                                            <p><i class="la la-envelope-o"></i> ali.tufan@jobhunt.com</p>
                                                        </div>-->
                            <input type="hidden" id="job-id" value="{{ $details->id }}" />
                            <a href="javascript:;" id="apply-for-job" class="apply-job-btn"><i class="la la-paper-plane"></i>{{ trans("lang.Apply for job") }}</a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
@stop