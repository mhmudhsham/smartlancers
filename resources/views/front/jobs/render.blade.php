@forelse($jobs as $one)
<div class="job-listing wtabs">
    <div class="job-title-sec">
        <div class="c-logo">
            <img src="{{ url('uploads/companies/'.$one->company->logo) }}" alt="{{ $one->job_title }}" />
        </div>
        <h3><a href="{{ url($lang.'/jobs/details/'.$one->id."/".str_replace(" ", "-", $one->job_title)) }}" title="{{ $one->job_title }}">{{ $one->job_title }}</a></h3>
        <span>{{ $one->company->{$slug->title} }}</span>
        @php($location = \App\City::find($one->job_location))
        <!--<div class="job-lctn"><i class="la la-map-marker"></i></div>-->
    </div>
    <div class="job-style-bx">
        <span class="job-is ft">{{ $one->jobtype->title_en }}</span>
        <!--<span class="fav-job"><i class="la la-heart-o"></i></span>-->
        <!--<i>5 months ago</i>-->
    </div>
</div>
@empty
@endforelse