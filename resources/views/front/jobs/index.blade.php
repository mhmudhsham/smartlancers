@extends("layouts.front")
@section("scripts")
<script src="{!! url('assets/front/js/validation/job.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<input type="hidden" id="hidden-field" value="0" />
<div class="theme-layout" id="scrollup">
    <section class="overlape">
        <div class="block no-padding">
            <div data-velocity="-.1" style="background: url({{ url('/assets/front/images/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner-header wform">
                            <div class="job-search-sec">
                                <div class="job-search">
                                    <h4>{{ trans("lang.Explore Thousand Of Jobs With Just Simple Search...") }}</h4>
                                    <form action="{{ url($lang.'/search_jobs') }}" method="post">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12">
                                                <div class="job-field">
                                                    <input type="text" name="title" placeholder="{{ trans("lang.Job title") }}" />
                                                    <i class="la la-keyboard-o"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                                                <div class="job-field">
                                                    <select name="location" data-placeholder="City, province or region" class="chosen-city">
                                                        @foreach($cities as $one)
                                                        <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                        @endforeach
                                                    </select>
                                                    <i class="la la-map-marker"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <button type="submit"><i class="la la-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="block remove-top">
            <div class="container">
                <div class="row no-gape">
                    <aside class="col-lg-3 column">


                        <div class="widget border">
                            <h3 class="sb-title open">{{ trans("lang.Job Type") }}</h3>
                            <div class="type_widget">
                                @foreach($types as $one)
                                <p class="flchek">
                                    <input data-id="{{ $one->id }}" type="checkbox" class="job_type" name="type_{{ $one->id }}" id="type_{{ $one->id }}">
                                    <label for="type_{{ $one->id }}">{{ $one->{$slug->title} }}</label>
                                </p>
                                @endforeach
                            </div>
                        </div>



                        <div class="banner_widget">
                            <a href="{{ url($lang.'/faq') }}" title=""><img src="{{ url('assets/front/images/banner.png') }}" alt="" /></a>
                        </div>
                    </aside>
                    <div class="col-lg-9 column">
                        <div class="job-list-modern">
                            <div class="job-listings-sec no-border">


                                <div id="all_jobs">
                                    @forelse($jobs as $one)
                                    <div class="job-listing wtabs">
                                        <div class="job-title-sec">
                                            <div class="c-logo">
                                                <img src="{{ url('uploads/companies/'.$one->company->logo) }}" alt="{{ $one->job_title }}" />
                                            </div>
                                            <h3><a href="{{ url($lang.'/jobs/details/'.$one->id."/".str_replace(" ", "-", $one->job_title)) }}" title="{{ $one->job_title }}">{{ $one->job_title }}</a></h3>
                                            <span>{{ $one->company->{$slug->title} }}</span>
                                            @php($location = \App\City::find($one->job_location))
                                            <!--<div class="job-lctn"><i class="la la-map-marker"></i></div>-->
                                        </div>
                                        <div class="job-style-bx">
                                            <span class="job-is ft">{{ $one->jobtype->title_en }}</span>
                                            <!--<span class="fav-job"><i class="la la-heart-o"></i></span>-->
                                            <!--<i>5 months ago</i>-->
                                        </div>
                                    </div>
                                    @empty
                                    @endforelse
                                </div>



                            </div>
                            <!--                            <div class="pagination">
                                                            <ul>
                                                                <li class="prev"><a href=""><i class="la la-long-arrow-left"></i> Prev</a></li>
                                                                <li><a href="">1</a></li>
                                                                <li class="active"><a href="">2</a></li>
                                                                <li><a href="">3</a></li>
                                                                <li><span class="delimeter">...</span></li>
                                                                <li><a href="">14</a></li>
                                                                <li class="next"><a href="">Next <i class="la la-long-arrow-right"></i></a></li>
                                                            </ul>
                                                        </div> Pagination -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@stop