@extends("layouts.front")
@section("scripts")
<!--<script src="{!! url('assets/front/js/validation/jquery.js') !!}" type="text/javascript"></script>-->
<script src="{!! url('assets/front/js/validation/index.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<section>
    <div class="block no-padding">
        <div class="container fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-featured-sec">
                        <ul class="main-slider-sec text-arrows">
                            <li class="slideHome"><img src="assets/front/images/slider/s1.jpg" alt="" /></li>
                            <li class="slideHome"><img src="assets/front/images/slider/s2.jpg" alt="" /></li>
                            <li class="slideHome"><img src="assets/front/images/slider/s3.jpg" alt="" /></li>
                        </ul>
                        <div class="job-search-sec">
                            <div class="job-search">
                                <h3>{{ trans("lang.Do You Need") }} " <span class="typed"></span> " ?!</h3>
                                <span>{{ trans("lang.Find Jobs, Employment & Career Opportunities") }}</span>
                                <form action="{{ url($lang.'/search_jobs') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-lg-7 col-md-5 col-sm-12 col-xs-12">
                                            <div class="job-field">
                                                <input type="text" name="title" placeholder="{{ trans("lang.Job title") }}" />
                                                <i class="la la-keyboard-o"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                                            <div class="job-field">
                                                <select name="location" data-placeholder="City, province or region" class="chosen-city">
                                                    @foreach($cities as $one)
                                                    <option value="{{ $one->id }}">{{ $one->{$slug->title} }}</option>
                                                    @endforeach
                                                </select>
                                                <i class="la la-map-marker"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12">
                                            <button type="submit"><i class="la la-search"></i></button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="scroll-to">
                            <a href="#scroll-here" title=""><i class="la la-arrow-down"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <h2>{{ trans("lang.Featured Jobs") }}</h2>
                        <span>{{ trans("lang.Leading Employers already using job and talent.") }}</span>
                    </div><!-- Heading -->
                    <div class="job-listings-sec">
                        @foreach($jobs as $one)
                        <div class="job-listing">
                            <div class="job-title-sec">
                                <div class="c-logo"> <img src="{{ url('uploads/companies/'.$one->company->logo) }}" alt="" /> </div>
                                <h3><a href="javascript:void(0)" title=""></a></h3>
                                <span>{{ $one->job_title }}</span>
                            </div>
                            <span class="job-lctn"><i class="la la-map-marker"></i>{{ $one->city->title_en }}</span>
                            <span class="fav-job"><i class="la la-heart-o"></i></span>
                            <span class="job-is ft">{{ $one->jobtype->title_en }}</span>
                        </div><!-- Job -->

                        @endforeach
                        <!--                        <div class="job-listing">
                                                    <div class="job-title-sec">
                                                        <div class="c-logo"> <img src="assets/front/images/l2.png" alt="" /> </div>
                                                        <h3><a href="#" title="">Marketing Director</a></h3>
                                                        <span>Tix Dog</span>
                                                    </div>
                                                    <span class="job-lctn"><i class="la la-map-marker"></i>Makaa</span>
                                                    <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                    <span class="job-is pt">PART TIME</span>
                                                </div> Job
                                                <div class="job-listing">
                                                    <div class="job-title-sec">
                                                        <div class="c-logo"> <img src="assets/front/images/l3.png" alt="" /> </div>
                                                        <h3><a href="#" title="">C Developer (Senior) C .Net</a></h3>
                                                        <span>StarHealth</span>
                                                    </div>
                                                    <span class="job-lctn"><i class="la la-map-marker"></i>Al-madina</span>
                                                    <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                    <span class="job-is ft">FULL TIME</span>
                                                </div> Job
                                                <div class="job-listing">
                                                    <div class="job-title-sec">
                                                        <div class="c-logo"> <img src="assets/front/images/l4.png" alt="" /> </div>
                                                        <h3><a href="#" title="">Application Developer For Android</a></h3>
                                                        <span>Altes Bank</span>
                                                    </div>
                                                    <span class="job-lctn"><i class="la la-map-marker"></i>Gadaa</span>
                                                    <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                    <span class="job-is fl">FREELANCE</span>
                                                </div> Job
                                                <div class="job-listing">
                                                    <div class="job-title-sec">
                                                        <div class="c-logo"> <img src="assets/front/images/l5.png" alt="" /> </div>
                                                        <h3><a href="#" title="">Regional Sales Manager South east Asia</a></h3>
                                                        <span>Vincent</span>
                                                    </div>
                                                    <span class="job-lctn"><i class="la la-map-marker"></i>Makaa</span>
                                                    <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                    <span class="job-is tp">TEMPORARY</span>
                                                </div> Job
                                                <div class="job-listing">
                                                    <div class="job-title-sec">
                                                        <div class="c-logo"> <img src="assets/front/images/l6.png" alt="" /> </div>
                                                        <h3><a href="#" title="">Social Media and Public Relation Executive </a></h3>
                                                        <span>MediaLab</span>
                                                    </div>
                                                    <span class="job-lctn"><i class="la la-map-marker"></i>Al-madina</span>
                                                    <span class="fav-job"><i class="la la-heart-o"></i></span>
                                                    <span class="job-is ft">FULL TIME</span>
                                                </div> Job -->
                    </div>
                </div>
                <!--                <div class="col-lg-12">
                                    <div class="browse-all-cat">
                                        <a href="#" title="">Load more Jobs</a>
                                    </div>
                                </div>-->
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <h2>How It Works</h2>
                        <span>Each month, more than 7 million Jobhunt turn to website in their search for work, making over <br />160,000 applications every day.
                        </span>
                    </div><!-- Heading -->
                    <div class="how-to-sec">
                        <div class="how-to">
                            <span class="how-icon"><i class="fa fa-lock"></i></span>
                            <h3>Register an account</h3>
                        </div>
                        <div class="how-to">
                            <span class="how-icon"><i class="fa fa-edit"></i></span>
                            <h3>Update Your CV</h3>
                        </div>
                        <div class="how-to">
                            <span class="how-icon"><i class="fa fa-envelope"></i></span>
                            <h3>Wait For E-mail Activation</h3>
                        </div>
                        <div class="how-to">
                            <span class="how-icon"><i class="fa fa-sticky-note"></i></span>
                            <h3>Apply for job</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block double-gap-top double-gap-bottom">
        <div data-velocity="-.1" style="background: url(assets/front/images/parallax1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible layer color"></div><!-- PARALLAX BACKGROUND IMAGE -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <h3>{{ trans("lang.Make a Difference with Your Online Resume!") }}</h3>
                        <span>{{ trans("lang.Your resume in minutes with JobHunt resume assistant is ready!") }}</span>
                        <a href="" class="signup-popup" title="">{{ trans("lang.Create an Account") }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="scroll-here">
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <h2>{{ trans("lang.Popular Categories") }}</h2>
                        <!--<span>37 jobs live - 0 added today.</span>-->
                    </div><!-- Heading -->
                    <div class="cat-sec">
                        <div class="row no-gape">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-bullhorn"></i>
                                        <span>Design, Art & Multimedia</span>
                                        <!--<p>(22 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-graduation-cap"></i>
                                        <span>Education Training</span>
                                        <!--<p>(6 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-line-chart "></i>
                                        <span>Accounting / Finance</span>
                                        <!--<p>(3 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-users"></i>
                                        <span>Human Resource</span>
                                        <!--<p>(3 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cat-sec">
                        <div class="row no-gape">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-phone"></i>
                                        <span>Telecommunications</span>
                                        <!--<p>(22 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-cutlery"></i>
                                        <span>Restaurant / Food Service</span>
                                        <!--<p>(6 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-building"></i>
                                        <span>Construction / Facilities</span>
                                        <!--<p>(3 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="p-category">
                                    <a href="javascript:;" title="">
                                        <i class="la la-user-md"></i>
                                        <span>Health</span>
                                        <!--<p>(3 open positions)</p>-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="block gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="subscription-sec">
                        <div class="row">
                            <div class="col-lg-6">
                                <h3>Still Need Help ?</h3>
                                <span>Subscribe Us and Let us now about your issue and a Professional will reach you out.</span>
                            </div>
                            <div class="col-lg-6">
                                <form id="subscribe_form">
                                    <input type="text" placeholder="{{ trans("lang.Enter Your Email Address") }}">
                                    <button type="submit"><i class="la la-paper-plane"></i></button>

                                </form>
                                <span id="help-block-subscribe"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop