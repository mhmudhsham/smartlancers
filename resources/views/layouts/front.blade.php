<!DOCTYPE html>
<html>
    <head>
        @include("components.front.meta")
    </head>
    <body>
        <div class="theme-layout" id="scrollup">
            @include("components.front.header")
            @yield("content")
            @include("components.front.sub_footer")
        </div>
        @include("components.front.footer")
        @yield("scripts")

    </body>
</html>
