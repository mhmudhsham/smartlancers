<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!--            <li class="sidebar-search-wrapper">
                            <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                <a href="javascript:;" class="remove">
                                    <i class="icon-close"></i>
                                </a>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <a href="javascript:;" class="btn submit">
                                            <i class="icon-magnifier"></i>
                                        </a>
                                    </span>
                                </div>
                            </form>
                        </li>-->
            <li class="nav-item">
                <a href="{{ url($lang."/settings") }}" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">{{ trans("lang.Settings") }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin") }}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">{{ trans("lang.Dashboard") }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/accounts") }}" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">{{ trans("lang.Manage_Accounts") }}</span>
                </a>
            </li>



            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">{{ trans("lang.Manage Training") }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/majors") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Majors") }}</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/instructors") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Instructors") }}</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/courses") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Courses") }}</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/materials") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Materials") }}</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/students") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Students") }}</span>
                        </a>
                    </li>




                </ul>
            </li>





            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-user"></i>
                    <span class="title">{{ trans("lang.Programs") }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">


                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/camps") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Programs") }}</span>
                        </a>
                    </li>
              
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/camp_reservations") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Reservations") }}</span>
                        </a>
                    </li>
                </ul>
            </li>






            <li class="nav-item">
                <a href="{{ url($lang."/admin/jobseekers") }}" class="nav-link nav-toggle">
                    <i class="icon-envelope"></i>
                    <span class="title">{{ trans("lang.Manage_Job_Seekers") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/jobs") }}" class="nav-link nav-toggle">
                    <i class="icon-bubbles"></i>
                    <span class="title">{{ trans("lang.Manage_Jobs") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/orders") }}" class="nav-link nav-toggle">
                    <i class="icon-bag"></i>
                    <span class="title">{{ trans("lang.Manage_Orders") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/companies") }}" class="nav-link nav-toggle">
                    <i class="icon-notebook"></i>
                    <span class="title">{{ trans("lang.Manage_Companies") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/pricing") }}" class="nav-link nav-toggle">
                    <i class="icon-briefcase"></i>
                    <span class="title">{{ trans("lang.Manage_Pricing_Plans") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/categories") }}" class="nav-link nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">{{ trans("lang.Manage_Categories") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/pages") }}" class="nav-link nav-toggle">
                    <i class="icon-chemistry"></i>
                    <span class="title">{{ trans("lang.Manage_Pages") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/emails") }}" class="nav-link nav-toggle">
                    <i class="icon-badge"></i>
                    <span class="title">{{ trans("lang.Manage_Emails") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/newsletters") }}" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">{{ trans("lang.Manage_Newsletters") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/subscribers") }}" class="nav-link nav-toggle">
                    <i class="icon-fire"></i>
                    <span class="title">{{ trans("lang.Manage_Subscribers") }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url($lang."/admin/faq") }}" class="nav-link nav-toggle">
                    <i class="icon-microphone"></i>
                    <span class="title">{{ trans("lang.Manage_FAQ") }}</span>
                </a>
            </li>
            <!--            <li class="heading">
                            <h3 class="uppercase">Features</h3>
                        </li>-->
            <li class="nav-item  ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">{{ trans("lang.Manage_Content_Fields") }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/universities") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Universities") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/cities") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Cities") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/countries") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Countries") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/specialists") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Specialists") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/industries") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Industries") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/jobtypes") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Job_Types") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/faqtypes") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_FAQ_Types") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/companytypes") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Company_Types") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/portaltags") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Portal_Tags") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/companyemployees") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Company_Employees") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/genders") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Gender") }}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ url($lang."/admin/graduationyears") }}" class="nav-link ">
                            <span class="title">{{ trans("lang.Manage_Graduation_Years") }}</span>
                        </a>
                    </li>

                </ul>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>