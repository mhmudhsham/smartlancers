<footer>
    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 column">
                    <div class="widget">
                        <div class="about_widget">
                            <div class="logo">
                                <a href="#" title=""><img src="{{ url('/assets/front/images/logo.png') }}" alt="" /></a>
                            </div>
                            <span>Maka - saudi arabia.</span>
                            <span>+966112222591</span>
                            <span>info@smartlancers.com</span>
                            <div class="social">
                                <a href="#" title="" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="#" title="" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="#" title="" target="_blank"><i class="fa fa-linkedin"></i></a>
                                <a href="#" title="" target="_blank"><i class="fa fa-pinterest"></i></a>
                                <a href="#" title="" target="_blank"><i class="fa fa-behance"></i></a>
                            </div>
                        </div><!-- About Widget -->
                    </div>
                </div>
                <div class="col-lg-4 column">
                    <div class="widget">
                        <h3 class="footer-title">Frequently Asked Questions</h3>
                        <div class="link_widgets">
                            <div class="row">
                                <div class="col-lg-6">
                                    <a href="#" title="">Privacy & Seurty </a>
                                    <a href="#" title="">Terms of Serice</a>
                                    <a href="#" title="">Communications </a>
                                    <a href="#" title="">Referral Terms </a>
                                    <a href="#" title="">Lending Licnses </a>
                                    <a href="#" title="">Disclaimers </a>
                                </div>
                                <div class="col-lg-6">
                                    <a href="#" title="">Support </a>
                                    <a href="#" title="">How It Works </a>
                                    <a href="#" title="">For Employers </a>
                                    <a href="#" title="">Underwriting </a>
                                    <a href="#" title="">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 column">
                    <div class="widget">
                        <h3 class="footer-title">Find Jobs</h3>
                        <div class="link_widgets">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="#" title="">Makaa Jobs</a>
                                    <a href="#" title="">Al-madina Jobs</a>
                                    <a href="#" title="">Al-reyad Jobs</a>
                                    <a href="#" title="">Gadaa en Fnce</a>
                                    <a href="#" title="">Jobs in Deuts</a>
                                    <a href="#" title="">Vacatures Al-madina</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 column">
                    <div class="widget">
                        <div class="download_widget">
                            <a href="#" title=""><img src="{{ url('/assets/front/images/dw1.png') }}" alt="" /></a>
                            <a href="#" title=""><img src="{{ url('/assets/front/images/dw2.png') }}" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-line">
        <span> All Rights Reserved @ 2018 &copy; Smart Lancers</span>
        <a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
    </div>
</footer>