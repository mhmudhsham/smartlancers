<div class="responsive-header">
    <div class="responsive-menubar">
        <div class="res-logo"><a href="#" title=""><img src="./assets/images/logo.png" alt="" /></a></div>
        <div class="menu-resaction">
            <div class="res-openmenu">
                <img src="./assets/images/icon.png" alt="" /> Menu
            </div>
            <div class="res-closemenu">
                <img src="./assets/images/icon2.png" alt="" /> Close
            </div>
        </div>
    </div>
    <div class="responsive-opensec">
        <div class="btn-extars">

              @php($session = Session::get("user_id"))
            @if($session == null)
            <!-- <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a> -->
            @endif
            <ul class="account-btns">
                @if($session == null)
                <li class="signup-popup"><a title=""><i class="la la-key"></i> {{ trans("lang.Sign Up") }}</a></li>
                <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> {{ trans("lang.Login") }}</a></li>
                @else
                <a href="{{ url($lang.'/courses/add') }}" title="" class="post-job-btn"><i class="la la-plus"></i>{{ trans("lang.Add Course") }}</a>
                <li class=""><a title="{{ trans("lang.profile") }}" href="{{ url($lang.'/profile') }}"><i class="la la-external-link-square"></i> {{ trans("lang.profile") }}</a></li>
                <!-- <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a> -->
                @endif
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                @if($lang != $localeCode)
                <li>

                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                        <i class="la la-language"></i>
                        {{ $properties['native'] }}
                    </a>
                </li>
                @endif
                @endforeach
                <!--<li class="signin-popup"><a title=""><i class="la la-language"></i> عربى</a></li>-->
            </ul>
        </div><!-- Btn Extras -->
        <form class="res-search">
            <input type="text" placeholder="Job title, keywords or company name" />
            <button type="submit"><i class="la la-search"></i></button>
        </form>
        <div class="responsivemenu">
            <ul>
                <li>
                    <a href="{{ url($lang."/") }}" title="">{{ trans("lang.Home") }}</a>
                </li>
                <li>
                    <a href="{{ url($lang."/jobs") }}" title="">{{ trans("lang.Jobs") }}</a>
                </li>
                <li>
                    <a href="{{ url($lang."/companies") }}" title="">{{ trans("lang.Companies") }}</a>
                </li>
                <li>
                    <a href="{{ url($lang."/courses") }}" title="">{{ trans("lang.Courses") }}</a>
                </li>
                <li>
                        <a href="{{ url($lang."/camps") }}" title="">{{ trans("lang.Programs") }}</a>
                    </li>
                <li>
                    <a href="{{ url($lang."/faq") }}" title="">{{ trans("lang.FAQ") }}</a>
                </li>
                <li>
                    <a href="{{ url($lang."/contact") }}" title="">{{ trans("lang.Contact Us") }}</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<header class="stick-top forsticky">
    <div class="menu-sec">
        <div class="container">
            <div class="logo">
                <a href="{{ url($lang."/") }}" title="">
                    <img class="hidesticky" src="{{ url('/assets/front/images/logo.png') }}" alt="" />
                    <img class="showsticky" src="{{ url('/assets/front/images/logo10.png') }}" alt="" />
                </a>
            </div><!-- Logo -->
            <div class="btn-extars">                
                @php($session = Session::get("user_id"))
                @if($session == null)
                
                @endif
                <ul class="account-btns">
                    @if($session == null)
                    <li class="signup-popup"><a title=""><i class="la la-key"></i> {{ trans("lang.Sign Up") }}</a></li>
                    <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> {{ trans("lang.Login") }}</a></li>
                    @else
                    @if(Session::get("company") == 0)
                    <li class=""><a href="{{ url($lang.'/courses/add') }}" title="" class=" "><i class="la la-plus"></i> {{ trans("lang.Course") }}</a></li>                    
                    <li class=""><a href="{{ url($lang.'/camps/add') }}" title="" class=" "><i class="la la-plus"></i> {{ trans("lang.Program") }}</a></li>                    
                    @endif   
                    <li class=""><a title="{{ trans("lang.profile") }}" href="{{ url($lang.'/profile') }}"><i class="la la-external-link-square"></i> {{ trans("lang.profile") }}</a></li>                                     
                    @endif
                        <!--<li class="signin-popup"><a title=""><i class="la la-language"></i> عربى</a></li>-->
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    @if($lang != $localeCode)
                    <li>
                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                            <i class="la la-language"></i>
                            {{ $properties['native'] }}
                        </a>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div><!-- Btn Extras -->
            <nav>
                <ul>
                    <li>
                        <a href="{{ url($lang."/") }}" title="">{{ trans("lang.Home") }}</a>
                    </li>
                    <li>
                        <a href="{{ url($lang."/jobs") }}" title="">{{ trans("lang.Jobs") }}</a>
                    </li>
                    <li>
                        <a href="{{ url($lang."/companies") }}" title="">{{ trans("lang.Companies") }}</a>
                    </li>
                    <li>
                        <a href="{{ url($lang."/courses") }}" title="">{{ trans("lang.Courses") }}</a>
                    </li>
                    <li>
                        <a href="{{ url($lang."/camps") }}" title="">{{ trans("lang.Programs") }}</a>
                    </li>
                    <li>
                        <a href="{{ url($lang."/faq") }}" title="">{{ trans("lang.FAQ") }}</a>
                    </li>
                    
                    <li>
                        <a href="{{ url($lang."/contact") }}" title="">{{ trans("lang.Contact Us") }}</a>
                    </li>
                </ul>
            </nav><!-- Menus -->
        </div>
    </div>
</header>