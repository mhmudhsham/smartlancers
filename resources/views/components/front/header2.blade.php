<div class="responsive-header">
    <div class="responsive-menubar">
        <div class="res-logo"><a href="#" title=""><img src="./assets/images/logo.png" alt="" /></a></div>
        <div class="menu-resaction">
            <div class="res-openmenu">
                <img src="./assets/images/icon.png" alt="" /> Menu
            </div>
            <div class="res-closemenu">
                <img src="./assets/images/icon2.png" alt="" /> Close
            </div>
        </div>
    </div>
    <div class="responsive-opensec">
        <div class="btn-extars">
            <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a>
            <ul class="account-btns">
                <li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li>
                <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Login</a></li>
                <li class="signin-popup"><a title=""><i class="la la-language"></i> عربى</a></li>
            </ul>
        </div><!-- Btn Extras -->
        <form class="res-search">
            <input type="text" placeholder="Job title, keywords or company name" />
            <button type="submit"><i class="la la-search"></i></button>
        </form>
        <div class="responsivemenu">
            <ul>
                    <li>
                        <a href="#" title="">Home</a>
                    </li>
                    <li>
                        <a href="#" title="">Jobs</a>
                    </li>
                    <li>
                        <a href="#" title="">Companies</a>
                    </li>
                    <li>
                        <a href="#" title="">Fqs</a>
                    </li>
                    <li>
                        <a href="#" title="">Contact Us</a>
                    </li>
                </ul>
        </div>
    </div>
</div>

<header class="stick-top forsticky">
    <div class="menu-sec">
        <div class="container">
            <div class="logo">
                <a href="#" title=""><img class="hidesticky" src="./assets/images/logo.png" alt="" /><img class="showsticky" src="./assets/images/logo10.png" alt="" /></a>
            </div><!-- Logo -->
            <div class="btn-extars">
                <a href="#" title="" class="post-job-btn"><i class="la la-plus"></i>Post Jobs</a>
                <ul class="account-btns">
                    <li class="signup-popup"><a title=""><i class="la la-key"></i> Sign Up</a></li>
                    <li class="signin-popup"><a title=""><i class="la la-external-link-square"></i> Login</a></li>
                    <li class="signin-popup"><a title=""><i class="la la-language"></i> عربى</a></li>
                </ul>
            </div><!-- Btn Extras -->
            <nav>
                <ul>
                    <li>
                        <a href="#" title=""><i class="fa fa-home"></i> Home</a>
                    </li>
                    <li>
                        <a href="#" title="">Jobs</a>
                    </li>
                    <li>
                        <a href="#" title="">Companies</a>
                    </li>
                    <li>
                        <a href="#" title="">Fqs</a>
                    </li>
                    <li>
                        <a href="#" title="">Contact Us</a>
                    </li>
                </ul>
            </nav><!-- Menus -->
        </div>
    </div>
</header>