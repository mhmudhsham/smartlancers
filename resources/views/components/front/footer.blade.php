<div class="account-popup-area job-popup-box">
    <div class="account-popup">
        <span class="close-popup"><i class="la la-close"></i></span>
        <h3>{{ trans("lang.User Login") }}</h3>
        <form>
            <div class="cfield">
                <input type="text" placeholder="{{ trans("lang.Username") }}" />
                <i class="la la-user"></i>
            </div>
            <div class="cfield">
                <input type="password" placeholder="********" />
                <i class="la la-key"></i>
            </div>
            <button type="submit">{{ trans("lang.Login") }}</button>
        </form>
    </div>
</div>

<div class="account-popup-area signin-popup-box">
    <div class="account-popup">
        <span class="close-popup"><i class="la la-close"></i></span>
        <h3>{{ trans("lang.User Login") }}</h3>
        <form id="login_form">
            <div class="cfield">
                <input type="text" placeholder="{{ trans("lang.Username") }}" />
                <i class="la la-user"></i>
            </div>
            <div class="cfield">
                <input type="password" placeholder="********" />
                <i class="la la-key"></i>
            </div>
            <button type="submit">{{ trans("lang.Login") }}</button>
            <span id="login-msg"></span>
        </form>
    </div>
</div><!-- LOGIN POPUP -->

<div class="account-popup-area signup-popup-box">
    <div class="account-popup">
        <span class="close-popup"><i class="la la-close"></i></span>
        <h3>{{ trans("lang.Sign Up") }}</h3>
        <div class="select-user">

            <span class="active" data-title="Candidate">{{ trans("lang.Candidate") }}</span>
            <span data-title="Employer">{{ trans("lang.Employer") }}</span>
            <span data-title="Instructor">{{ trans("lang.Instructor") }}</span>
        </div>
        <form id="register_form" type="post">
            {{ csrf_field() }}
            <div class="cfield">
                <input type="text" name="username" id="username" placeholder="{{ trans("lang.Username") }}" />
                <i class="la la-user"></i>
            </div>
            <div class="cfield">
                <input type="password" name="password" id="password" placeholder="********" />
                <i class="la la-key"></i>
            </div>
            <div class="cfield">
                <input type="text" id="email" name="email" placeholder="{{ trans("lang.Email") }}" />
                <i class="la la-envelope-o"></i>
            </div>
            <div class="cfield">
                <input type="text" id="phone" name="phone" placeholder="{{ trans("lang.Phone Number") }}" />
                <i class="la la-phone"></i>
            </div>
            <button type="submit">{{ trans("lang.Sign up") }}</button>
            <span id="register-msg"></span>
        </form>
    </div>
</div><!-- SIGNUP POPUP -->

<script src="{!! url('assets/front/js/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/bootstrap.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/modernizr.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/wow.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/slick.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/parallax.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/typed.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/select-chosen.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/front/js/script.js') !!}" type="text/javascript"></script>
<script type="text/javascript">
        $(function() {
        $(".typed").typed({
        strings: [lang.You_know_what_you_should_do, lang.Use_your_own, lang.Have_a_great_day],
                typeSpeed: 190
        });
        });
</script>

<!--uploadfilejs-->
<script src="{!! url('assets/front/css/bootstrap-fileinput/js/plugins/piexif.js') !!}"></script>
<script src="{!! url('assets/front/css/bootstrap-fileinput/js/fileinput.min.js') !!}"></script>
<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script type="text/javascript" src="{!! url('assets/front/css/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}"></script>
@if($lang == "ar")
<script src="{!! url('assets/front/css/bootstrap-material-datetimepicker/js/locales/ar.js') !!}"></script>
@endif

<script>
$(".uploadfile").fileinput({
    uploadUrl: "/site/image-upload",
    autoReplace: true,
    maxFileCount: 5,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
	$('.date').bootstrapMaterialDatePicker({ weekStart : 0, time: false });
</script>