<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Smart Lancers</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Smart Lancers">
    <meta name="keywords" content="Smart Lancers">
    <meta name="author" content="Smart Lancers">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons -->
    <link rel="icon" href="{!! url('assets/front/images/favicon.ico') !!}">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/bootstrap.min.css') !!}" />
    @if($lang == "ar")
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/bootstrap-rtl.css') !!}" />
    @endif
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/icons.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/animate.min.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/font-awesome.min.css') !!}">
    @if($lang == "en")
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/custom_en.css') !!}" />
    @else
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/custom_ar.css') !!}" />
    @endif
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/responsive.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/chosen.css') !!}" />
    <link rel="stylesheet" type="text/css" href="{!! url('assets/front/css/colors/colors.css') !!}" />
    
    <!-- new styles of courses -->
    <link href="{!! url('assets/front/css/bootstrap-fileinput/css/fileinput.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    @if($lang == "ar")
    <link href="{!! url('assets/front/css/bootstrap-fileinput/css/fileinput-rtl.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
    @endif
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{!! url('assets/front/css/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') !!}" />
    <!-- end of courses -->
    
    <script type="text/javascript">
        var config = {
            base_url: "{{ url($lang.'/') }}",
            site_url: "{{ url('/') }}",
        };
        var lang = {
            sent_successfully: "{{ trans('lang.sent_successfully') }}",
            this_field_is_required: "{{ trans('lang.this_field_is_required') }}",
            deleted_successfully: "{{ trans('lang.deleted_successfully') }}",
            email_not_valid: "{{ trans('lang.email_not_valid') }}",
            not_number: "{{ trans('lang.not_number') }}",
            url_not_valid: "{{ trans('lang.url_not_valid') }}",
            already_exists: "{{ trans('lang.already_exists') }}",
            false_credintials: "{{ trans('lang.false_credintials') }}",
            fill_all_fields: "{{ trans('lang.fill_all_fields') }}",
            successfully_applied: "{{ trans('lang.successfully_applied') }}",
            login_first: "{{ trans('lang.login_first') }}",
            You_know_what_you_should_do: "{{ trans('lang.You_know_what_you_should_do') }}",
            Use_your_own: "{{ trans('lang.Use_your_own') }}",
            Have_a_great_day: "{{ trans('lang.Have_a_great_day') }}",
        };
    </script>
</head>