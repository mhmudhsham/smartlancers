@extends("layouts.admin")
@section("page_title", trans("lang.Add New Instructor"))
@section("page_header_title", "")
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}" rel="stylesheet" type="text/css" />
@end
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/form-validation-md.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/mine/instructor.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{ trans("lang.Add New Instructor") }}</span>
                </div>
            </div>
            <div class="portlet-body">
                @if($id == 0)
                @php($action = url('/admin/instructors/store'))
                @else
                @php($action = url('/admin/instructors/update'). "/" . $id)
                @endif
                <!-- BEGIN FORM   -->
                <form action="{{ $action }}" class="form-horizontal" id="instructor_form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="id" value="{{ $id }}" />
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.You_have_some_form_errors._Please_check_below.") }} </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.Your_form_validation_is_successful!") }} </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Logo") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                    <div>
                                        <span class="btn red btn-outline btn-file">
                                            <span class="fileinput-new"> {{ trans("lang.Select New") }} </span>
                                            <span class="fileinput-exists"> {{ trans("lang.Change") }} </span>
                                            <input type="file" name="logo" id="logo" > </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{ trans("lang.Remove") }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Name") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="name" id="name">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_name") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Email") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="email" id="email">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_Email") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.username") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="username" id="username">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_username") }}</span>
                            </div>
                        </div>



                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.password") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" placeholder="" name="password" id="password">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_password") }}</span>
                            </div>
                        </div>



                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.phone") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="phone" id="phone">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_phone") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.experience") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="experience" id="experience">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_experience") }}</span>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.bio") }}
                                <span class="required"> </span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="bio" id="bio"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_bio") }}</span>
                            </div>
                        </div>





                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ trans("lang.save") }}</button>
                                <button type="reset" class="btn default">{{ trans("lang.clear") }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>

</div>
@stop