@extends("layouts.admin")
@section("page_title", trans("lang.FAQ Data"))
@section("page_header_title", "")
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}" rel="stylesheet" type="text/css" />
@end
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/form-validation-md.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/mine/faq.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{ trans("lang.FAQ Data") }}</span>
                </div>
            </div>
            <div class="portlet-body">
                @if($id == 0)
                @php($action = url('/admin/faq/store'))
                @else
                @php($action = url('/admin/faq/update'). "/" . $id)
                @endif
                <!-- BEGIN FORM   -->
                <form action="{{ $action }}" class="form-horizontal" id="faq_form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="id" value="{{ $id }}" />
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.You_have_some_form_errors._Please_check_below.") }} </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.Your_form_validation_is_successful!") }} </div>



                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Choose FAQ type") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" name="faqtype_id" id="faqtype_id">
                                    <option value="">{{ trans("lang.Choose FAQ type") }}</option>
                                    @foreach($types as $one)
                                    <option value="{{ $one->id }}">{{ $one->title_ar }}</option>
                                    @endforeach
                                </select>

                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Choose FAQ type") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.question_ar") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="question_ar" id="question_ar"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_question_ar") }}</span>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.question_en") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="question_en" id="question_en"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_question_en") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.answer_ar") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="answer_ar" id="answer_ar"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_answer_ar") }}</span>
                            </div>
                        </div>

                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.answer_en") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="answer_en" id="answer_en"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_answer_en") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ trans("lang.save") }}</button>
                                <button type="reset" class="btn default">{{ trans("lang.clear") }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>

</div>
@stop