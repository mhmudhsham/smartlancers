@extends("layouts.admin")
@section("page_title",  trans("lang.All_Jobs"))
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/bootstrap-toastr/toastr.min.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/global/plugins/icheck/skins/all.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
@if($lang == "ar")
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') !!}" rel="stylesheet" type="text/css" />
@else
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}" rel="stylesheet" type="text/css" />
@endif
@stop
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/bootstrap-toastr/toastr.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/icheck/icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/scripts/datatable.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/datatables.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/ui-toastr.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/pages/scripts/form-icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/pages/scripts/table-datatables-responsive.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/mine/job.js') !!}"></script>
@stop

@section("content")
<!--<div class="m-heading-1 border-green m-bordered">
</div>-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase">
                        <!--<a class="btn btn-default" href="{{ url('/admin/jobseekers/add') }}">Add New</a>-->
                    </span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                    <thead>
                        <tr>
                            <th class="all">{{ trans("lang.ID") }}</th>
                            <th class="all"> {{ trans("lang.Title") }} </th>
                            <th class="all"> {{ trans("lang.Salary") }} </th>
                            <th class="all"> {{ trans("lang.Working_Hours") }} </th>
                            <th class="all"> {{ trans("lang.Status") }} </th>
                            <th class="all"> {{ trans("lang.Features") }} </th>
                            <!--<th class="all">{{ trans("lang.Action") }}</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        @php($counter = 1)
                        @foreach($rows as $one)
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td>{{ $one->job_title }}</td>
                            <td>{{ $one->salary }}</td>
                            <td>{{ $one->working_hours }} {{ trans("lang.Hours") }}</td>
                            <td class="text-center">
                                @php($checked = "")
                                @if($one->status == 1)
                                @php($checked = "checked")
                                @endif
                                <input class=" status-checkbox" data-id='{{ $one->id }}' type="checkbox" name="status" value="{{ $one->status }}" {{ $checked }}  />
                            </td>
                            <td class="text-center">
                                @php($checked = "")
                                @if($one->featured == 1)
                                @php($checked = "checked")
                                @endif
                                <input class=" featured-checkbox" data-id='{{ $one->id }}' type="checkbox" name="featured" value="{{ $one->featured }}" {{ $checked }}  />
                            </td>
<!--                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">{{ trans("lang.Tools") }}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ url("/admin/jobseekers/edit/")."/".$one->id }}">
                                                <i class="fa fa-pencil-square-o"></i> {{ trans("lang.Edit") }} </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>-->
                        </tr>
                        @php($counter++)
                        @endforeach
                    </tbody>
                </table>
                {{ $rows->links() }}
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop
