@extends("layouts.admin")
@section("page_title",  trans("lang.All_Company_Employees") )
@section("page_header_title", "")
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
@if($lang == "ar")
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') !!}" rel="stylesheet" type="text/css" />
@else
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}" rel="stylesheet" type="text/css" />
@endif
@stop
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/scripts/datatable.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/datatables.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/table-datatables-responsive.min.js') !!}" type="text/javascript"></script>
@stop

@section("content")
<!--<div class="m-heading-1 border-green m-bordered">-->

<!--</div>-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase">
                        <!--<a class="btn btn-default" href="{{ url('/admin/accounts/add') }}">Add New</a>-->
                    </span>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                    <thead>
                        <tr>
                            <th class="all">{{ trans("lang.ID") }}</th>
                            <th class="all">{{ trans("lang.Employees_Count") }}</th>
                            <!--<th class="all">{{ trans("lang.Actions") }}</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        @php($counter = 1)
                        @foreach($rows as $one)
                        <tr>
                            <td>{{ $counter }}</td>
                            <td>{{ $one->employees_count }}</td>
<!--                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">{{ trans("lang.Tools") }}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ url("/admin/companyemployees/edit/")."/".$one->id }}">
                                                <i class="fa fa-pencil-square-o"></i> {{ trans("lang.Edit") }}</a>
                                        </li>
                                        <li>
                                            <a href="{{ url("/admin/companyemployees/delete/")."/".$one->id }}">
                                                <i class="fa fa-trash-o"></i> {{ trans("lang.Delete") }} </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>-->
                        </tr>
                        @php($counter++)
                        @endforeach
                    </tbody>
                </table>
                {{ $rows->links() }}
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop