@extends("layouts.admin")
@section("page_title", trans("lang.View Camp") )
@section("page_header_title", "")


@section("content")
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">


            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                    <col width="30%" />
                    <col width="70%" />
                    <tbody>
                        <tr>
                            <th class="text-center">{{ trans("lang.Title (AR)") }}</th>
                            <td class="text-center">{{ $details->title_ar }}</td>
                        </tr>
                        <tr>
                            <th class="text-center">{{ trans("lang.Title (EN)") }}</th>
                            <td class="text-center">{{ $details->title_en }}</td>
                        </tr>
                        <tr>
                            <th class="text-center">{{ trans("lang.Description (AR)") }}</th>
                            <td class="text-center">{{ $details->description_ar }}</td>
                        </tr>
                        <tr>
                            <th class="text-center">{{ trans("lang.Description (EN)") }}</th>
                            <td class="text-center">{{ $details->description_en }}</td>
                        </tr> 
                        <tr>
                            <th class="text-center">{{ trans("lang.Price") }}</th>
                            <td class="text-center">{{ $details->price }}</td>
                        </tr>
                        <tr>
                            <th class="text-center">{{ trans("lang.Start Date") }}</th>
                            <td class="text-center">{{ $details->start_date }}</td>
                        </tr>
                        <tr>
                            <th class="text-center">{{ trans("lang.End Date") }}</th>
                            <td class="text-center">{{ $details->end_date }}</td>
                        </tr>                        
                        <tr>
                            <th class="text-center">{{ trans("lang.Status") }}</th>
                            <td class="text-center">{{ $details->is_active }}</td>
                        </tr>                         
                        <tr>
                            <th class="text-center">{{ trans("lang.Created") }}</th>
                            <td class="text-center">{{ $details->created_at }}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop
