@extends("layouts.admin")
@section("page_title", trans("lang.All_Companies") )
@section("page_header_title", "")
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/bootstrap-toastr/toastr.min.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/global/plugins/icheck/skins/all.css') !!}" rel="stylesheet" type="text/css" />
<link href="{!! url('assets/admin/global/plugins/datatables/datatables.min.css') !!}" rel="stylesheet" type="text/css" />
@if($lang == "ar")
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css') !!}" rel="stylesheet" type="text/css" />
@else
<link href="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') !!}" rel="stylesheet" type="text/css" />
@endif
@stop
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/bootstrap-toastr/toastr.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/icheck/icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/scripts/datatable.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/datatables.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/ui-toastr.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/pages/scripts/form-icheck.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/pages/scripts/table-datatables-responsive.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/mine/company.js') !!}"></script>
@stop

@section("content")
<!--<div class="m-heading-1 border-green m-bordered">-->

<!--</div>-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase">
                        <a class="btn btn-info" href="{{ url($lang.'/admin/companies/create') }}">{{ trans("lang.Add_New") }}</a>
                    </span>
                </div>
                <div class="tools"> </div>
            </div>

            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                    <thead>
                        <tr>
                            <th class="all">{{ trans("lang.ID") }}</th>
                            <!--<th class="all">{{ trans("lang.Logo") }}</th>-->
                            <th class="all"> {{ trans("lang.Company_Name") }}</th>
                            <th class="all"> {{ trans("lang.Company_Email") }}</th>
                            <th class="all"> {{ trans("lang.phone") }}</th>
                            <th class="all"> {{ trans("lang.Status") }}</th>
                            <th class="all">{{ trans("lang.Action") }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($counter = 1)
                        @foreach($rows as $one)
                        <tr class="tr_{{ $one->id }}">
                            <td class="text-center">{{ $counter }}</td>
                            <!--<td><img src="{{ url('/uploads/companies/'.$one->logo) }}" style="width: 50px;height: 50px;" /></td>-->
                            <td>{{ $one->title_ar }}</td>
                            <td>{{ $one->email }}</td>
                            <td>{{ $one->phone_number }}</td>
                            <!--<td class="text-center"><a target="_blank" href="{{ url($lang.'/admin/jobs/companies/')."/".$one->id }}">{{ trans("lang.Jobs") }}</a> </td>-->
                            <!--<td class="text-center"><a href="{{ url($lang.'/admin/companies/packages/')."/".$one->id }}" target="_blank">{{ trans("lang.Packages") }}</a> </td>-->
                            <td class="text-center">
                                @php($checked = "")
                                @if($one->status == 1)
                                @php($checked = "checked")
                                @endif
                                <input class=" status-checkbox" data-id='{{ $one->id }}' type="checkbox" name="status" value="{{ $one->status }}" {{ $checked }}  />
                            </td>
                            <td>
                                <div class="btn-group pull-right">
                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">{{ trans("lang.Tools") }}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="{{ url($lang."/admin/companies/edit/")."/".$one->id }}">
                                                <i class="fa fa-pencil-square-o"></i> {{ trans("lang.Edit") }}</a>
                                        </li>
                                        <li>
                                            <a class="delete-url" data-id="{{ $one->id }}" href="{{ url("/admin/companies/destroy/")."/".$one->id }}">
                                                <i class="fa fa-trash-o"></i> {{ trans("lang.Delete") }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @php($counter++)
                        @endforeach
                    </tbody>
                </table>
                {{ $rows->links() }}
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop
