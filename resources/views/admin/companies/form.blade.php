@extends("layouts.admin")
@section("page_title", trans("lang.Add_New_Company"))
@section("page_header_title", "")
@section("page_level_styles")
<link href="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') !!}" rel="stylesheet" type="text/css" />
@end
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/jquery-validation/js/additional-methods.min.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/form-validation-md.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/mine/company.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">{{ trans("lang.Add_New_Company") }}</span>
                </div>
            </div>
            <div class="portlet-body">
                @if($id == 0)
                @php($action = url('/admin/companies/store'))
                @else
                @php($action = url('/admin/companies/update'). "/" . $id)
                @endif
                <!-- BEGIN FORM   -->
                <form action="{{ $action }}" class="form-horizontal" id="company_form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="id" value="{{ $id }}" />
                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.You_have_some_form_errors._Please_check_below.") }} </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> {{ trans("lang.Your_form_validation_is_successful!") }} </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Logo") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                    <div>
                                        <span class="btn red btn-outline btn-file">
                                            <span class="fileinput-new"> {{ trans("lang.Select New") }} </span>
                                            <span class="fileinput-exists"> {{ trans("lang.Change") }} </span>
                                            <input type="file" name="logo" id="logo" > </span>
                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{ trans("lang.Remove") }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input ">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Title_(AR)") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="title_ar" id="title_ar">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_title_(AR)") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Title_(EN)") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="title_en" id="title_en">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_title_(EN)") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Choose Pricing Plan") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" name="priceplan_id" id="priceplan_id">
                                    <option value="">{{ trans("lang.Choose Pricing Plan") }}</option>
                                    @foreach($pricing as $one)
                                    <option value="{{ $one->id }}">{{ $one->title_ar }}</option>
                                    @endforeach
                                </select>

                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Choose Pricing Plan") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Choose company employee") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" name="companyemployee_id" id="companyemployee_id">
                                    <option value="">{{ trans("lang.Choose company employee") }}</option>
                                    @foreach($companyemployees as $one)
                                    <option value="{{ $one->id }}">{{ $one->employees_count }}</option>
                                    @endforeach
                                </select>

                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Choose company employee") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Choose company type") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" name="companytype_id" id="companytype_id">
                                    <option value="">{{ trans("lang.Choose company type") }}</option>
                                    @foreach($companytypes as $one)
                                    <option value="{{ $one->id }}">{{ $one->title_ar }}</option>
                                    @endforeach
                                </select>

                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Choose company type") }}</span>
                            </div>
                        </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.Choose Status") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <select class="form-control" name="status">
                                    <option value="1">{{ trans("lang.Active") }}</option>
                                    <option value="0">{{ trans("lang.In Active") }}</option>
                                </select>

                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Choose company type") }}</span>
                            </div>
                        </div>




                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.user_fullname") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="user_fullname" id="user_fullname">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_user_fullname") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.user_email") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="user_email" id="user_email">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_user_email") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.user_password") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" placeholder="" name="user_password" id="user_password">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_user_password") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.package_expirydate") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="package_expirydate" id="package_expirydate">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_package_expirydate") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.phone_number") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="phone_number" id="phone_number">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_phone_number") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.email") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="email" id="email">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_email") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.website") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="website" id="website">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_website") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.the_order") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" placeholder="" name="the_order" id="the_order">
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_the_order") }}</span>
                            </div>
                        </div>
                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">{{ trans("lang.description") }}
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="4" name="description" id="description"></textarea>
                                <div class="form-control-focus"> </div>
                                <span class="help-block">{{ trans("lang.Enter_description") }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">{{ trans("lang.save") }}</button>
                                <button type="reset" class="btn default">{{ trans("lang.clear") }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>

</div>
@stop