@extends("layouts.admin")
@section("page_title", "Dashboard")
@section("page_level_script_plugins")
<script src="{!! url('assets/admin/global/plugins/morris/morris.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/morris/raphael-min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/counterup/jquery.waypoints.min.js') !!}" type="text/javascript"></script>
<script src="{!! url('assets/admin/global/plugins/counterup/jquery.counterup.min.js') !!}" type="text/javascript"></script>
@stop
@section("page_level_scripts")
<script src="{!! url('assets/admin/pages/scripts/dashboard.min.js') !!}" type="text/javascript"></script>
@stop
@section("content")
<div class="row">
    <div class="text-center" style="margin: -25px 0 5px 0;">
        <h2 class="btn btn-default">{{ trans("lang.Today_Statistics") }}</h2>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $dayData['daySales'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Day_Sales") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $dayData['dayJobseekers'] }}">0</span> </div>
                <div class="desc"> {{ trans("lang.Day_Job_Seekers") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $dayData['dayCompanies'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Day_Companies") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $dayData['dayJobs'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Day_Jobs") }} </div>
            </div>
        </a>
    </div>
</div>


<div class="row">
    <div class="text-center" style="margin: -25px 0 5px 0;">
        <h2 class="btn btn-default">{{ trans("lang.Week_Statistics") }}</h2>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $weekData['weekSales'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Week_Sales") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $weekData['weekJobseekers'] }}">0</span> </div>
                <div class="desc"> {{ trans("lang.Week_Job_Seekers") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $weekData['weekCompanies'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Week_Companies") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $weekData['weekJobs'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Week_Jobs") }} </div>
            </div>
        </a>
    </div>
</div>


<div class="row">
    <div class="text-center" style="margin: -25px 0 5px 0;">
        <h2 class="btn btn-default">{{ trans("lang.Month_Statistics") }}</h2>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $monthData['monthSales'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Month_Sales") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $monthData['monthJobseekers'] }}">0</span> </div>
                <div class="desc"> {{ trans("lang.Month_Job_Seekers") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $monthData['monthCompanies'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Month_Companies") }} </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="javascript:void(0);">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $monthData['monthJobs'] }}">0</span>
                </div>
                <div class="desc"> {{ trans("lang.Month_Jobs") }} </div>
            </div>
        </a>
    </div>
</div>
@stop